<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();
Route::get('/user/verify/{token}', 'Auth\RegisterController@verifyUser');

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/about', 'HomeController@about')->name('about');
Route::get('/contact-us', 'HomeController@contact_us')->name('contact-us');
Route::post('/contact-us', 'HomeController@emailSend')->name('contact-us');
Route::get('/term-of-use', 'HomeController@termOfUse')->name('term-of-use');
Route::get('/privacy-policy', 'HomeController@privacyPolicy')->name('privacy-policy');


//for post requirement
Route::group(['middleware' => 'admin'], function () {
    Route::get('/post-requirement-list', 'PostRequirementController@index')->name('post-requirement-list');
});
Route::group(['middleware' => 'client'], function () {
	Route::get('/create-post-requirement', 'PostRequirementController@create')->name('create-post-requirement');
	Route::post('/create-post-requirement', 'PostRequirementController@store')->name('create-post-requirement');
});

//post project by partner
Route::get('/create-project', 'PostProjectController@create')->name('create-project');;
Route::post('/create-project', 'PostProjectController@store')->name('create-project');;

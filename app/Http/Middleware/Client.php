<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class Client 
{
    public function handle($request, Closure $next)
    {
        if ( Auth::user()->role_id == 2 ) {
            return $next($request);
        } else {
            return redirect()->route('home');
        }

    }
}


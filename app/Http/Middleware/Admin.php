<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class Admin 
{
    public function handle($request, Closure $next)
    {
        if ( Auth::user()->role_id == 1 ) {
            return $next($request);
        } else if(Auth::user()->role_id == 3 ) {
            return redirect()->route('home');
        } else {
        	return redirect()->route('create-post-requirement');
        }

    }
}


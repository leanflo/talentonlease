<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendMailable;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    public function about() 
    {
        return view('about');
    }

    public function contact_us()
    {
        return view('contact_us');
    }

    public function termOfUse()
    {
        return view('terms-of-use');
    }

    public function privacyPolicy()
    {
        return view('privacy-policy');
    }

    public function emailSend(Request $request)
    {
        $data = $request->all();
        
        Mail::to('sumitkaushik101@gmail.com')->send(new SendMailable($data));
        
        if (Mail::failures()) {
            Session::flash('error', "Your mail not send!");
            return Redirect::back();
        } else {
            Session::flash('message', "Your mail send successfully!");
            return Redirect::back();
        }

    }

}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PostProject;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use Auth;
use Intervention\Image\Facades\Image;
use App\City;
use App\State;
use App\Industry;


class PostProjectController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    protected function validator(array $data)
    {
        $rules = [
            'job_title' => 'required',
            'job_description' => 'required',
            'mandatory_skills' => 'required',
            'optional_skills' => 'required',
            'num_of_position' => 'required',
            'engagement_type' => 'required',
            'location' => 'required',
            'start_date' => 'required',
            'end_date' => 'required',
            'employment_type' => 'required',
            'industry' => 'required',
            'qualification' => 'required',
            'contact_person_name' => 'required',
            'contact_person_phone' => 'required|numeric'
        ];

        $messages = [
            'no_admin' => 'The name admin is restricted for :attribute'
        ];

        return Validator::make($data, $rules, $messages);
    }

    public function index()
    {	
    	$posts = PostRequirement::latest()->get();
        //return view('post-requirement/index', compact('posts'))->with('no', 1)->render();
    }

    public function create()
    {   
        $industries = Industry::get();
        $cities = City::get();
        return view('post-project/create', compact('cities', 'industries'));
    }

    public function store(Request $request)
    {
        $data = $request->all();

        $validator = $this->validator($request->all());

        if ($validator->fails()) {
            return redirect()->back()
                  ->withInput($request->all())
                  ->withErrors($validator->errors());
        }
        if(isset($request->mandatory_skills)) {
            $data['mandatory_skills'] = implode(', ', $request->mandatory_skills);    
        }
        if(isset($request->optional_skills)) {
            $data['optional_skills'] = implode(', ', $request->optional_skills);    
        }
        if(isset($request->location)) {
            $data['location'] = implode(', ', $request->location);    
        }
        
        $post = PostProject::create($data);
    
        if($post) {
            Session::flash('success', "Profile added successfully!");
            return Redirect::back();
        } else {
            Session::flash('error', "Profile not added!");
            return Redirect::back();
        }
        
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PostRequirement;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use Auth;
use Intervention\Image\Facades\Image;
use App\City;
use App\State;
use App\Industry;
use App\Category;
use App\SubCategory;

class PostRequirementController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    protected function validator(array $data)
    {
        $rules = [
            'title' => 'required',
            'applicant_type' => 'required',
            'num_of_position' => 'required',
            'category' => 'required',
            'sub_category' => 'required',
            'exp_required' => 'required',
            'qualification' => 'required',
            'industry' => 'required',
            'assignment_detail' => 'required',
            'assignment_duration' => 'required',
            'talent_required' => 'required',
            'work_nature' => 'required',
            'location' => 'required',
            'project_start_date' => 'required',
            'project_last_date' => 'required',
            'assignment_doc' => 'mimes:doc,docx,pdf|max:2048'
        ];

        $messages = [
            'no_admin' => 'The name admin is restricted for :attribute'
        ];

        return Validator::make($data, $rules, $messages);
    }

    public function index()
    {	
    	$posts = PostRequirement::latest()->get();
    	//$posts = PostRequirement::where('user_id', '=', Auth::id())->latest()->get();
        //return view('post-requirement/index');
        return view('post-requirement/index', compact('posts'))->with('no', 1)->render();
    }

    public function create()
    {   
        $industries = Industry::get();
        $categories = Category::get();
        $sub_categories = SubCategory::get();
        $cities = City::get();
        return view('post-requirement/create', compact('cities', 'industries','categories','sub_categories'));
    }

    public function store(Request $request)
    {
        $data = $request->all();

        $validator = $this->validator($request->all());

        if ($validator->fails()) {
            return redirect()->back()
                  ->withInput($request->all())
                  ->withErrors($validator->errors());
        }
        $postdoc = '';
        if ($request->file('assignment_doc')) { 
             	$postImage = $request->file('assignment_doc');
                $strippedName = str_replace(' ', '', $postImage->getClientOriginalName());
                $photoName = date('Y-m-d-H-i-s').$strippedName;

                //$postdoc = Image::make($postImage->getRealPath());
                //save orignal
                // if (!file_exists(storage_path().'/uploads/post-requirement/')) {
                //     mkdir(storage_path().'/uploads/post-requirement/', 0777, true);
                // }
                //$postImage->save(storage_path().'/uploads/post-requirement/'.$photoName);
                $postImage->move(public_path().'/uploads/post-requirement/', $photoName);
                $postdoc = $photoName;

        }

        if(isset($request->sub_category)) {
            $data['sub_category'] = implode(', ', $request->sub_category);    
        }
        if(isset($request->location)) {
            $data['location'] = implode(', ', $request->location);    
        }
        if($postdoc != '') {
        	$data['assignment_doc'] = $postdoc;
        } else {
                $data['assignment_doc'] = '';
        }
        
        $post = PostRequirement::create($data);
        
        // Flash::success(trans('Post requirement add successfully!'));
        // return redirect()->back();
       
        if($post) {
            Session::flash('success', "Post requirement add successfully!");
            return Redirect::back();
        } else {
            Session::flash('error', "Post requirement not add!");
            return Redirect::back();
        }
        
    }
}

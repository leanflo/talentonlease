<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Session;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */

    protected function authenticated($request, $user)
    {
        if (!$user->verified) {
            auth()->logout();
            return back()->with('warning', 'You need to confirm your account. We have sent you an activation code, please check your email.');
        }
        //return redirect()->intended($this->redirectPath());
         if($user->role_id == 1){ 
             return redirect('/post-requirement-list');
         }else if($user->role_id == 3){ 
            //Session::flash('success', "Welcome to Talent on Lease! Our team will get in touch with you in next 48 hours to guide you through the on-boarding process and complete your profile. Thanks,Partner Onboarding Team, Talent on Lease.");
             return redirect('/home');
         } else {
            //Session::flash('success', "Welcome to Talent on Lease! Now you can post your requirement.");
            return redirect('/create-post-requirement');
         }
    }

    //protected $redirectTo = '/create-post-requirement';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
}

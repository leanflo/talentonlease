<?php

namespace App;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;

class PostProject extends Model
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'post_projects';

    protected $fillable = [
        'job_title','user_id','job_description', 'mandatory_skills', 'optional_skills','num_of_position','min_work_experience','max_work_experience','min_budget', 'max_budget','engagement_type','location','start_date','end_date','employment_type','industry','qualification','contact_person_name','contact_person_phone'
    ];

}

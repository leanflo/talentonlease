<?php

namespace App;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;

class PostRequirement extends Model
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'post_requirement';

    protected $fillable = [
        'title','user_id','applicant_type', 'num_of_position', 'category','sub_category','industry','exp_required','qualification', 'assignment_detail','skill_required','assignment_duration','talent_required','work_nature','location','project_start_date','project_last_date','budget_type','min_budget','max_budget','assignment_doc'
    ];

}

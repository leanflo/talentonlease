@extends('layouts.app') @section('content')
<style>
    body {
        color: #232370;
    }
    
    .wrapper {
        margin-top:0px !important;
    }
</style>

<div class="wrapper">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h6 class="description">ABOUT US</h6>

                <p class="texts"><b>Talent on Lease (TOL) </b>is a new age online platform backed by who’s who of IT industry, created by industry veterans with a single motto “Making available <b>Right Resources</b> at <b> Right Time </b> at  <b>Right Cost</b>”. TOL has access to a wide range of clients across the country who have urgent IT requirements for critical projects on a regular basis. TOL works with a wide range of partners (IT Services/Solution firms) to fulfil these requirements. All the IT partners we have on the platform have been on-boarded after careful verification and vetting.
                </p>
                <p class="texts">
                   TOL is aimed at addressing the problem faced by the industry by bringing together Clients and Partners on a single platform to collaborate for mutual gains. The platform here acts as a responsible medium to not just provide means to transact for its tenants but also ensuring the trust building between them with the help of strong policy framework as well as strict audit mechanism.
                </p>
            </div>
        </div>
    </div>
</div>

</div>
</br>
<section class="">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="col-lg-12">
                    <h6 class="description">Founding Team</h6>
                    <div class="row pt-md">

                        <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 profile">
                            <div class="img-box">
                                <img src="img/p1.jpg" class="img-responsive img-thumbnail">
                                <ul class="text-center">
                                    <!--<a href="javascript:void(0)">
                                        <li><i class="fa fa-facebook"></i></li>
                                    </a>
                                    <a href="javascript:void(0)">
                                        <li><i class="fa fa-twitter"></i></li>
                                    </a>
                                    <a href="https://www.linkedin.com/in/dayaprakash/">
                                        <li><i class="fa fa-linkedin"></i></li>
                                    </a>-->
                                </ul>
                            </div>
                            <h1><a href="https://www.linkedin.com/in/dayaprakash/"target="_blank">Daya Prakash</a></h1>
                            <h2>Founder</h2>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 profile">
                            <div class="img-box">
                                <img src="img/p2.jpg" class="img-responsive img-thumbnail">
                                <ul class="text-center">
                                    <!-- <a href="javascript:void(0)">
                                        <li><i class="fa fa-facebook"></i></li>
                                    </a>
                                    <a href="javascript:void(0)">
                                        <li><i class="fa fa-twitter"></i></li>
                                    </a> 
                                    <a href="https://www.linkedin.com/in/pervesh-dhingra-2359167/">
                                        <li><i class="fa fa-linkedin"></i></li>
                                    </a>-->
                                </ul>
                            </div>
                            <h1><a href="https://www.linkedin.com/in/pervesh-dhingra-2359167/">Pervesh Dhingra</a></h1>
                            <h2>Co-Founder</h2>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 profile">
                            <div class="img-box">
                                <img src="img/p3.jpg" class="img-responsive img-thumbnail">
                                <ul class="text-center">
                                    <!--<a href="javascript:void(0)">
                                        <li><i class="fa fa-facebook"></i></li>
                                    </a>
                                    <a href="javascript:void(0)">
                                        <li><i class="fa fa-twitter"></i></li>
                                    </a>
                                    <a href="https://www.linkedin.com/in/sanjeev-matta-0391005/">
                                        <li><i class="fa fa-linkedin"></i></li>
                                    </a>-->
                                </ul>
                            </div>
                            <h1><a href="https://www.linkedin.com/in/sanjeev-matta-0391005/">Sanjeev Matta</a></h1>
                            <h2>Co-Founder</h2>
                        </div>

                        <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 profile">
                            <div class="img-box">
                                <img src="img/p4.jpg" class="img-responsive img-thumbnail">
                                <ul class="text-center">
                                    <!--<a href="javascript:void(0)">
                                        <li><i class="fa fa-facebook"></i></li>
                                    </a>
                                    <a href="javascript:void(0)">
                                        <li><i class="fa fa-twitter"></i></li>
                                    </a>
                                    <a href="https://www.linkedin.com/in/aniruddha-roy-a0837a128/">
                                        <li><i class="fa fa-linkedin"></i></li>
                                    </a>-->
                                </ul>
                            </div>
                            <h1><a href="https://www.linkedin.com/in/aniruddha-roy-a0837a128/">Aniruddha Roy</a></h1>
                            <h2>Co-Founder</h2>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="col-lg-12">
                    <h6 class="description">Advisory Panel</h6>
                    <div class="row pt-md">

                        <div class="col-lg-2 col-md-2 col-sm-4 col-xs-6 profile">
                            <div class="img-box">
                                <img src="img/a1.jpg" class="img-responsive img-thumbnail">
                                <ul class="text-center">
                                    <!--<a href="javascript:void(0)">
                                        <li><i class="fa fa-facebook"></i></li>
                                    </a>
                                    <a href="javascript:void(0)">
                                        <li><i class="fa fa-twitter"></i></li>
                                    </a>
                                    <a href="https://www.linkedin.com/in/suhasmhaskar/"target="_blank">
                                        <li><i class="fa fa-linkedin"></i></li>
                                    </a>-->
                                </ul>
                            </div>
                            <h2><b><a href="https://www.linkedin.com/in/suhasmhaskar/"target="_blank">Suhas Mhaskar</a></b></h2>
                            <h2>Ex. CIO, Mahindra & Mahindra</h2>
                        </div>

                        <div class="col-lg-2 col-md-2 col-sm-4 col-xs-6 profile">
                            <div class="img-box">
                                <img src="img/a2.jpg" class="img-responsive img-thumbnail">
                                <ul class="text-center">
                                    <!--<a href="javascript:void(0)">
                                        <li><i class="fa fa-facebook"></i></li>
                                    </a>
                                    <a href="javascript:void(0)">
                                        <li><i class="fa fa-twitter"></i></li>
                                    </a>
                                    <a href="https://www.linkedin.com/in/sanjeevcio/"target="_blank">
                                        <li><i class="fa fa-linkedin"></i></li>
                                    </a>-->
                                </ul>
                            </div>
                            <h2><b><a href="https://www.linkedin.com/in/sanjeevcio/"target="_blank">Sanjeev Kumar</a></b></h2>
                            <h2>Ex. Director IT, Philips India & South East Asia</h2>
                        </div>

                        <div class="col-lg-2 col-md-2 col-sm-4 col-xs-6 profile">
                            <div class="img-box">
                                <img src="img/a3.jpg" class="img-responsive img-thumbnail">
                                <ul class="text-center">
                                    <!--<a href="javascript:void(0)">
                                        <li><i class="fa fa-facebook"></i></li>
                                    </a>
                                    <a href="javascript:void(0)">
                                        <li><i class="fa fa-twitter"></i></li>
                                    </a>
                                    <a href="https://www.linkedin.com/in/avinash-arora-192118/"target="_blank">
                                        <li><i class="fa fa-linkedin"></i></li>
                                    </a>-->
                                </ul>
                            </div>
                            <h2><b><a href="https://www.linkedin.com/in/avinash-arora-192118/"target="_blank">Avinash Arora</a></b></h2>
                            <h2>Ex. Director IT andSCM, New Holland Tractor & Fiat India Pvt. Ltd</h2>
                        </div>

                        <div class="col-lg-2 col-md-2 col-sm-4 col-xs-6 profile">
                            <div class="img-box">
                                <img src="img/a4.jpg" class="img-responsive img-thumbnail">
                                <ul class="text-center">
                                    <!--<a href="javascript:void(0)">
                                        <li><i class="fa fa-facebook"></i></li>
                                    </a>
                                    <a href="javascript:void(0)">
                                        <li><i class="fa fa-twitter"></i></li>
                                    </a>
                                    <a href="https://www.linkedin.com/in/ranendra-datta-a442625/"target="_blank">
                                        <li><i class="fa fa-linkedin"></i></li>
                                    </a>-->
                                </ul>
                            </div>
                            <h2><b><a href="https://www.linkedin.com/in/ranendra-datta-a442625/"target="_blank">Ranendra Datta</a></b></h2>
                            <h2>Ex. VP & CIO, SABMiller</h2>
                            <h2>.</h2>
                            </br>
                        </div>

                        <div class="col-lg-2 col-md-2 col-sm-4 col-xs-6 profile">
                            <div class="img-box">
                                <img src="img/a5.jpg" class="img-responsive img-thumbnail">
                                <ul class="text-center">
                                    <!--<a href="javascript:void(0)">
                                        <li><i class="fa fa-facebook"></i></li>
                                    </a>
                                    <a href="javascript:void(0)">
                                        <li><i class="fa fa-twitter"></i></li>
                                    </a>-->
                                    <a href="https://www.linkedin.com/in/amarindersingh/"target="_blank">
                                        <li><i class="fa fa-linkedin"></i></li>
                                    </a>-->
                                </ul>
                            </div>
                            <h2><b><a href="https://www.linkedin.com/in/amarindersingh/"target="_blank">Amarinder Singh</a></b></h2>
                            <h2>CIO, CIO Association of India</h2>
                        </div>

                        <div class="col-lg-2 col-md-2 col-sm-4 col-xs-6 profile">
                            <div class="img-box">
                                <img src="img/a6.jpg" class="img-responsive img-thumbnail">
                                <ul class="text-center">
                                    <!--<a href="javascript:void(0)">
                                        <li><i class="fa fa-facebook"></i></li>
                                    </a>
                                    <a href="javascript:void(0)">
                                        <li><i class="fa fa-twitter"></i></li>
                                    </a>
                                    <a href="https://www.linkedin.com/in/t-g-dhandapani-b624708/""target="_blank">
                                        <li><i class="fa fa-linkedin"></i></li>
                                    </a>-->
                                </ul>
                            </div>
                            <h2><b> <a href="https://www.linkedin.com/in/t-g-dhandapani-b624708/""target="_blank">T G Dhandapani</a></b></h2>
                            <h2>Ex. Group CIO, TVS Motors and Sundaram Clayton Group</h2>
                        </div>

                    </div>

                    <div class="row pt-md">

                        <div class="col-lg-2 col-md-2 col-sm-4 col-xs-6 profile">
                            <div class="img-box">
                                <img src="img/a7.jpg" class="img-responsive img-thumbnail">
                                <ul class="text-center">
                                    <!--<a href="javascript:void(0)">
                                        <li><i class="fa fa-facebook"></i></li>
                                    </a>
                                    <a href="javascript:void(0)">
                                        <li><i class="fa fa-twitter"></i></li>
                                    </a>
                                    <a href="https://www.linkedin.com/in/sudhirparya/""target="_blank">
                                        <li><i class="fa fa-linkedin"></i></li>
                                    </a>-->
                                </ul>
                            </div>
                            <h2><b><a href="https://www.linkedin.com/in/sudhirparya/""target="_blank">S P Arya</a></b></h2>
                            <h2>Ex. CIO, Amtek Group</h2>
                        </div>
                        <div class="col-lg-2 col-md-2 col-sm-4 col-xs-6 profile">
                            <div class="img-box">
                                <img src="img/a8.jpg" class="img-responsive img-thumbnail">
                                <ul class="text-center">
                                    <!--<a href="javascript:void(0)">
                                        <li><i class="fa fa-facebook"></i></li>
                                    </a>
                                    <a href="javascript:void(0)">
                                        <li><i class="fa fa-twitter"></i></li>
                                    </a>
                                    <a href="https://www.linkedin.com/in/anshul-dureja/""target="_blank">
                                        <li><i class="fa fa-linkedin"></i></li>
                                    </a>-->
                                </ul>
                            </div>
                            <h2><b> <a href="https://www.linkedin.com/in/anshul-dureja/""target="_blank">Anshul Dureja</a></b></h2>
                            <h2>Ex. CIO, GreenplyIndustries Limited</h2>
                        </div>
                        <div class="col-lg-2 col-md-2 col-sm-4 col-xs-6 profile">
                            <div class="img-box">
                                <img src="img/a9.jpg" class="img-responsive img-thumbnail">
                                <ul class="text-center">
                                    <!--<a href="javascript:void(0)">
                                        <li><i class="fa fa-facebook"></i></li>
                                    </a>
                                    <a href="javascript:void(0)">
                                        <li><i class="fa fa-twitter"></i></li>
                                    </a>
                                    <a href="https://www.linkedin.com/in/kinshuk-hora-6557301/""target="_blank">
                                        <li><i class="fa fa-linkedin"></i></li>
                                    </a>-->
                                </ul>
                            </div>
                            <h2><b><a href="https://www.linkedin.com/in/kinshuk-hora-6557301/""target="_blank">Kinshuk Hora</a></b></h2>
                            <h2>Ex. GM IT, GSK</h2>
                        </div>
                        <div class="col-lg-2 col-md-2 col-sm-4 col-xs-6 profile">
                            <div class="img-box">
                                <img src="img/a10.jpg" class="img-responsive img-thumbnail">
                                <ul class="text-center">
                                    <!--<a href="javascript:void(0)">
                                        <li><i class="fa fa-facebook"></i></li>
                                    </a>
                                    <a href="javascript:void(0)">
                                        <li><i class="fa fa-twitter"></i></li>
                                    </a>
                                    <a href="https://www.linkedin.com/in/atulluthra/""target="_blank">
                                        <li><i class="fa fa-linkedin"></i></li>
                                    </a>-->
                                </ul>
                            </div>
                            <h2><b> <a href="https://www.linkedin.com/in/atulluthra/""target="_blank">Atul Luthra</a></b></h2>
                            <h2>Ex. CIO, PVR Cinemas & Matrix Cellular International</h2>
                        </div>
                        <div class="col-lg-2 col-md-2 col-sm-4 col-xs-6 profile">
                            <div class="img-box">
                                <img src="img/a11.jpg" class="img-responsive img-thumbnail">
                                <ul class="text-center">
                                    <!--<a href="javascript:void(0)">
                                        <li><i class="fa fa-facebook"></i></li>
                                    </a>
                                    <a href="javascript:void(0)">
                                        <li><i class="fa fa-twitter"></i></li>
                                    </a>
                                    <a href="https://www.linkedin.com/in/atulluthra/""target="_blank">
                                        <li><i class="fa fa-linkedin"></i></li>
                                    </a>-->
                                </ul>
                            </div>
                            <h2><b> <a href="https://www.linkedin.com/in/atulluthra/""target="_blank">Rahul Neelmani</a></b></h2>
                            <h2>Cofounder & Editor, Grey Head Media</h2>
                        </div>
                        <div class="col-lg-2 col-md-2 col-sm-4 col-xs-6 profile">
                            <div class="img-box">
                                <img src="img/a12.jpg" class="img-responsive img-thumbnail">
                                <ul class="text-center">
                                    <!--<a href="javascript:void(0)">
                                        <li><i class="fa fa-facebook"></i></li>
                                    </a>
                                    <a href="javascript:void(0)">
                                        <li><i class="fa fa-twitter"></i></li>
                                    </a>
                                    <a href="https://www.linkedin.com/in/manuhaar/""target="_blank">
                                        <li><i class="fa fa-linkedin"></i></li>
                                    </a>-->
                                </ul>
                            </div>
                            <h2><b> <a href="https://www.linkedin.com/in/manuhaar/""target="_blank">Manuhaar Agarwall</a></b></h2>
                            <h2>Ex. GM IT, TPG Wholsale</h2>
                        </div>

                    </div>

                    <div class="row pt-md">
                        <div class="col-lg-2 col-md-2 col-sm-4 col-xs-6 profile">
                            <div class="img-box">
                                <img src="img/a13.jpg" class="img-responsive img-thumbnail">
                                <ul class="text-center">
                                    <!--<a href="javascript:void(0)">
                                        <li><i class="fa fa-facebook"></i></li>
                                    </a>
                                    <a href="javascript:void(0)">
                                        <li><i class="fa fa-twitter"></i></li>
                                    </a>
                                    <a href="https://www.linkedin.com/in/swaranjit-s-soni-a665356/""target="_blank">
                                        <li><i class="fa fa-linkedin"></i></li>
                                    </a>-->
                                </ul>
                            </div>
                            <h2><b><a href="https://www.linkedin.com/in/swaranjit-s-soni-a665356/""target="_blank">S S Soni</a></b></h2>
                            <h2>	Ex. Executive Director (IT), IOCL</h2>
                        </div>

                        <div class="col-lg-2 col-md-2 col-sm-4 col-xs-6 profile">
                            <div class="img-box">
                                <img src="img/a14.jpg" class="img-responsive img-thumbnail">
                                <ul class="text-center">
                                    <!--<a href="javascript:void(0)">
                                        <li><i class="fa fa-facebook"></i></li>
                                    </a>
                                    <a href="javascript:void(0)">
                                        <li><i class="fa fa-twitter"></i></li>
                                    </a>
                                    <a href="https://www.linkedin.com/in/pankajmittal1/""target="_blank">
                                        <li><i class="fa fa-linkedin"></i></li>
                                    </a>-->
                                </ul>
                            </div>
                            <h2><b><a href="https://www.linkedin.com/in/pankajmittal1/""target="_blank">Pankaj Mittal</a></b></h2>
                            <h2>Ex. Head Datacenter Transformation, Vodafone</h2>
                        </div>

                        <div class="col-lg-2 col-md-2 col-sm-4 col-xs-6 profile">
                            <div class="img-box">
                                <img src="img/a15.jpg" class="img-responsive img-thumbnail">
                                <ul class="text-center">
                                    <!--<a href="javascript:void(0)">
                                        <li><i class="fa fa-facebook"></i></li>
                                    </a>
                                    <a href="javascript:void(0)">
                                        <li><i class="fa fa-twitter"></i></li>
                                    </a>
                                    <a href="https://www.linkedin.com/in/hiteshkarora/""target="_blank">
                                        <li><i class="fa fa-linkedin"></i></li>
                                    </a>-->
                                </ul>
                            </div>
                            <h2><b><a href="https://www.linkedin.com/in/hiteshkarora/""target="_blank">Hitesh Arora</a></b></h2>
                            <h2>Ex. CIO, Max Life Insurance</h2>
                        </div>

                        <div class="col-lg-2 col-md-2 col-sm-4 col-xs-6 profile">
                            <div class="img-box">
                                <img src="img/a16.jpg" class="img-responsive img-thumbnail">
                                <ul class="text-center">
                                    <!--<a href="javascript:void(0)">
                                        <li><i class="fa fa-facebook"></i></li>
                                    </a>
                                    <a href="javascript:void(0)">
                                        <li><i class="fa fa-twitter"></i></li>
                                    </a>
                                    <a href="https://www.linkedin.com/in/lssubramanian/""target="_blank">
                                        <li><i class="fa fa-linkedin"></i></li>
                                    </a>-->
                                </ul>
                            </div>
                            <h2><b>  <a href="https://www.linkedin.com/in/lssubramanian/""target="_blank">L S Subramanian</a></b></h2>
                            <h2>Ex. Director IT, CRISIL</h2>
                        </div>

                        <div class="col-lg-2 col-md-2 col-sm-4 col-xs-6 profile">
                            <div class="img-box">
                                <img src="img/a17.jpg" class="img-responsive img-thumbnail">
                                <ul class="text-center">
                                    <!--<a href="javascript:void(0)">
                                        <li><i class="fa fa-facebook"></i></li>
                                    </a>
                                    <a href="javascript:void(0)">
                                        <li><i class="fa fa-twitter"></i></li>
                                    </a>
                                    <a href="https://www.linkedin.com/in/parvinder-singh-2436115/""target="_blank">
                                        <li><i class="fa fa-linkedin"></i></li>
                                    </a>-->
                                </ul>
                            </div>
                            <h2><b><a href="https://www.linkedin.com/in/parvinder-singh-2436115/""target="_blank">Parvinder Singh</a></b></h2>
                            <h2>	Ex. Head IT Infrastrucuture, Fortis Hospital</h2>
                        </div>
                        <div class="col-lg-2 col-md-2 col-sm-4 col-xs-6 profile">
                            <div class="img-box">
                                <img src="img/a18.jpg" class="img-responsive img-thumbnail">
                                <ul class="text-center">
                                    <!--<a href="javascript:void(0)">
                                        <li><i class="fa fa-facebook"></i></li>
                                    </a>
                                    <a href="javascript:void(0)">
                                        <li><i class="fa fa-twitter"></i></li>
                                    </a>
                                    <a href="https://www.linkedin.com/in/mrparmindersingh/""target="_blank">
                                        <li><i class="fa fa-linkedin"></i></li>
                                    </a>-->
                                </ul>
                            </div>
                            <h2><b><a href="https://www.linkedin.com/in/mrparmindersingh/""target="_blank">Parvinder Singh</a></b></h2>
                            <h2>Ex. Sr. VP and Global CIO, Jubilant</h2>
                        </div>

                    </div>

                </div>
            </div>
        </div>
    </div>
</section>

@endsection
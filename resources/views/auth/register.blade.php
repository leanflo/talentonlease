@extends('layouts.app')

@section('content')
<style>
.block-con{padding-top: 0px !important;}
</style>

<div class="block-con">
    <div class="col-sm-3 reg-left col-xs-12 hidden-xs">

    </div>
    <div class="col-sm-6 reg-right col-xs-12 float-r">
        <h2 class="reg_heading">Signup </h2>
        <div role="tabpanel" class="tab-pane active" id="sme_profile">
            <form method="POST" action="{{ route('register') }}" aria-label="{{ __('Register') }}">
                        @csrf
                <div class="row  margintop">
                    <div class="col-md-12"> 
                        <div class="form-group">
                            <label class="radio-inline" for="partner">
                            <input type="radio" name="role_id" value="3" checked="checked" @if(old('role_id') == 3) checked @endif>Partner</label>   
                            <label class="radio-inline" for="client">
                            <input type="radio" name="role_id" value="2" @if(old('role_id') == 2) checked @endif>Client</label> 
                      </div>
                    </div>

                    <div class="col-md-6 ">
                        <div class="form-group">
                            <label for="company_name">Company Name <span style="color:red;">*</span></label>
                            <input id="company_name" type="text" class="form-control{{ $errors->has('company_name') ? ' is-invalid' : '' }}" name="company_name" value="{{ old('company_name') }}" autofocus>

                            @if ($errors->has('company_name'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('company_name') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-6 ">
                        <div class="form-group">
                            <!-- <label for="industry">Industry <span style="color:red;">*</span></label>
                            <input id="industry" type="text" class="form-control{{ $errors->has('industry') ? ' is-invalid' : '' }}" name="industry" value="{{ old('industry') }}"> -->

                            <label for="industry">Industry <span style="color:red;">*</span></label>
                            <select class="form-control req_field" id="industry" name="industry">
                                <option value="" selected="selected">Select One</option>
                                @foreach ($industries as $industry)
                                  <option value="{{ $industry->id }}" {{(old('industry') == $industry->id?'selected':'')}}>{{ $industry->name}}</option>
                                @endforeach
                            </select>

                            @if ($errors->has('industry'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('industry') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                </div>
                <div class="row">
                    <div class="col-md-6 ">
                        <div class="form-group">
                            <label for="first_name"> First Name <span style="color:red;">*</span></label>
                           <input id="first_name" type="text" class="form-control{{ $errors->has('first_name') ? ' is-invalid' : '' }}" name="first_name" value="{{ old('first_name') }}">

                            @if ($errors->has('first_name'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('first_name') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-6 ">
                        <div class="form-group">
                            <label for="last_name"> Last Name <span style="color:red;">*</span></label>
                           <input id="last_name" type="text" class="form-control{{ $errors->has('last_name') ? ' is-invalid' : '' }}" name="last_name" value="{{ old('last_name') }}">

                            @if ($errors->has('last_name'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('last_name') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 ">
                        <div class="form-group">
                            <label for="email"> Email ID<span style="color:red;">*</span></label>
                            <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}">

                            @if ($errors->has('email'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="col-md-6 ">
                        <div class="form-group">
                            <label for="contact_number"> Mobile Number <span style="color:red;">*</span></label>
                            <input id="contact_number" type="text" maxlength="10"class="form-control{{ $errors->has('contact_number') ? ' is-invalid' : '' }}" name="contact_number" value="{{ old('contact_number') }}">

                            @if ($errors->has('contact_number'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('contact_number') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 ">
                        <div class="form-group">
                            <label for="password"> Password <span style="color:red;">*</span></label>
                            <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" value="{{ old('password') }}">

                            @if ($errors->has('password'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-6 ">
                        <div class="form-group">
                            <label for="confirm_password"> Confirm Password <span style="color:red;">*</span></label>
                           <input id="password-confirm" type="password" class="form-control" name="password_confirmation">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 ">
                        <div class="form-group">
                            <label for="country"> Country <span style="color:red;">*</span></label>
                            <input id="country" type="text" class="form-control" name="country" value="India" readonly>
                        </div>
                    </div>

                    <div class="col-md-6 ">
                        <div class="form-group">
                            <label for="city" class="text-uppercase">City<span style="color:red;">*</span></label>
                            <select class="form-control" name="city">
                                <option value="">Select City</option>
                                @foreach ($cities as $city)
                                  <option value="{{ $city->id }}" {{(old('city') == $city->id?'selected':'')}}>{{ $city->name}}</option>
                                @endforeach
                            </select>

                            @if ($errors->has('city'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('city') }}</strong>
                                </span>
                            @endif

                        </div>
                    </div>

                    <div class="col-md-12 ">
                        <label for="terms"></label>
                        <div class="checkbox" style="margin-top:5px;font-size: 13px;">
                            <input id="id_accept_terms_conditions" name="accept_terms_conditions" type="checkbox" value="1">
                            <label for="id_accept_terms_conditions">I agree with the <a href="{{ url('/term-of-use') }}">Terms of Use</a> and <a href="{{ url('/privacy-policy') }}">Privacy policy</a> </label><br>

                            @if ($errors->has('accept_terms_conditions'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('accept_terms_conditions') }}</strong>
                                </span>
                            @endif
                        </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12  text-center">
                        <button type="submit" class="btn marginbottom btn-primary btn-md btnRegister register_org" style="margin-left:10px;background:#165198 !important">Signup</button>

                    </div>
                </div>

                <div class="row">

                </div>
            </form>
        </div>

    </div>
    <div class="col-md-12 text-center">

        <!-- <a href="{{ url('/login') }}">
            <div class="btn btn-primary btnRegister btn-md" style="background:#232370!important;    margin-top: -66px;color:white;border: 2px solid #232370!important;padding: 4px 50px;">Already Registered? Login</div>
        </a> -->
        <a href="{{ url('/login') }}"><div class="btn btn-primary btnRegister btn-md" style="background:#fff !important;    margin-top: -103px;color:#232370;;border: 1px solid#232370;!important;padding: 4px 50px;">Already Registered? Login</div></a>
    </div>
    <div class="col-sm-3 reg-left col-xs-12 float-l hidden-xs">

    </div>
</div>
@endsection

@extends('layouts.app') @section('content')
<style>
    body {
        color: #232370;
    }
    
    .wrapper {
        margin-top: 100px !important;
    }
</style>
<div class="wrapper">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h6 class="description">TERMS OF USE AGREEMENT</h6>

                <p class="texts">
                    These terms of use (this “Agreement”) set forth the standards of use of Talent on Lease and its affiliates and subsidiaries (referred to herein as " Talent on Lease ", "we", "our" or "us") located at www.talentonlease.com and all of its associated pages, related sub-domains, websites, services and tools or any successor site collectively referred to herein as the “Website”.
                </p>
                <p class="texts">
                    The words “You” or “User” as used herein, refers to all individuals and/or entities accessing or using the Website for any reason. By using the Website, You represent that You have read and agree to be bound by the terms of this Agreement, as well as any other guidelines, privacy policy, rules and additional terms referenced herein, collectively referred to as “Terms of Use”. These Terms of Use set out the legally binding terms with respect to Your access and use of the Website provision of the Services (as defined below). Please read these Terms of Use carefully. Your access to Website and/or use of the Service constitutes Your acceptance of all the provisions of these Terms of Use. If You are unwilling to be bound by these Terms of Use, do not access Website and/or use the Service.
                </p>
                <h4>1. Definitions.</h4>
                <p class="texts">
                    (i) “Account” means the account successfully opened by the User on the Website by inserting information such as name, registration details (if a juristic person), contact details, user name, password as required to be filled in the webpage during the Registration Process and includes any further changes and additions to the information from time to time.
                </p>
                <p class="texts">
                    (ii) “Skill Provider” means any User who accesses the Website or uses the Services with an intention to secure projects/ assignments.
                </p>
                <p class="texts">
                    (iii) “Opportunity-Provider” means any company or any other entity which accesses the Website or uses the Services with an intention to provide projects / assignments to the eligible Skill Providers in its own organisation or in facilitating Skill Providers to get projects / assignments in any other organisation for a term as detailed by it.
                </p>
                <p class="texts">
                    (iv) “Member” means the User who has completed the Registration Process successfully as per clause 6 of this Agreement.
                </p>
                
                <h4>2. Eligibility.</h4>

                <p class="texts">
                    By checking any acceptance boxes, clicking any acceptance buttons, submitting any text or content or simply by making any use of the Website, you and accessing our Website you 
                </p>
                <p class="texts">
                    (i) accept the Terms of Use that appear below and agree to be bound by each of its terms, and 
                </p>
                <p class="texts">
                    (ii) represent and warrant to Talent on Lease that: (a) You are at least eighteen (18) years of age, or the required legal age in your jurisdiction, and have the authority to enter into this Agreement; (b) this Agreement is binding and enforceable against you; (c) to the extent an individual is accepting this Agreement on behalf of an entity, such individual has the right and authority to agree to all of the terms set forth herein on behalf of such entity; and (d) You have read and understand Talent on Lease’s Privacy Policy, the terms of which are posted at the Website and incorporated herein by reference (the "Privacy Policy"), and agree to abide by the Privacy Policy. 
                </p>
                <h4>3. Services.</h4>
                <p class="texts">
                    3.1 The use of this Website entitles the User, whether a Skill Provider or an OpportunityProvider, to avail certain services as provided in the following clauses (“Services”) and interpretation of the term “Services” shall be done accordingly depending upon the context.
                </p>
                <p class="texts">
                    3.2 If the Skill Providers have completed registration as per clause 6.1, they shall be entitled to search for a project / assignment and view project / assignment listings and snapshots of the projects / assignments
                </p>
                <p class="texts">
                    3.3 If the Opportunity-Providers have completed simple registration as per clause 6.2, they shall be entitled for listing projects / assignments and after the screening, verification and approval of the same they can communicate with the Skill Providers.
                </p>
                <p class="texts">
                    3.4 The Website reserves the right to change the nature of Services as mentioned in clause 3.2 or clause 3.3 at its sole discretion. Such change may be notified to the User by publishing the same on the Website.
                </p>
                
                <h4>4. Use of the Website.</h4>
                <p class="texts">
                    4.1 You agree, undertake and confirm that your use of the Website shall be strictly governed by the following binding principles:
                </p>
                <p class="texts">
                    i. You shall not host, display, upload, modify, publish, transmit, update or share any information or item that:
                </p>
                <p class="texts">
                    a. belongs to another person and to which you do not have any right to;
                </p>
                <p class="texts">
                    b. is grossly harmful, harassing, blasphemous, defamatory, obscene,
                    pornographic;
                </p>
                <p class="texts">
                    c. pedophilic, libelous, invasive of another's privacy, hateful, or racially,
                    ethnically objectionable, disparaging, relating or encouraging money
                    laundering or gambling, or otherwise unlawful in any manner whatever;
                </p>
                <p class="texts">
                    d. harms minors in any way;
                </p>
                <p class="texts">
                    e. infringes any patent, trademark, copyright or other proprietary rights;
                </p>
                <p class="texts">
                    f. violates any law for the time being in force;
                </p>
                <p class="texts">
                    g. deceives or misleads the addressee/ users about the origin of such
                    messages or communicates any information which is grossly offensive or
                    menacing in nature;
                </p>
                <p class="texts">
                    h. impersonates another person;
                </p>
                <p class="texts">
                    i. contains software viruses or any other computer code, files or programs
                    designed to interrupt, destroy or limit the functionality of any computer
                    resource;
                </p>
                <p class="texts">
                    j. threatens the unity, integrity, defence, security or sovereignty of India,
                    friendly relations with foreign states, or public order or causes incitement
                    to the commission of any cognizable offence or prevents investigation of
                    any offence or is insulting any other nation;
                </p>
                <p class="texts">
                    k. shall not be false, inaccurate or misleading;
                </p>
                <p class="texts">
                    l. shall not create liability for us or cause us to lose (in whole or in part) the
                    services of our ISPs or other suppliers; and
                </p>
                <p class="texts">
                    ii. You enable Talent on Lease to use the information you supply us with ("Information"), so that we are not violating any rights you might have in your information, you agree to grant us a non-exclusive, worldwide, perpetual, irrevocable, royalty-free, sub-licensable (through multiple tiers) right to exercise the copyright, publicity, and database rights (but no other rights) you have in your Information, in any media now known or not currently known, with respect to your Information. Talent on Lease will only use your Information in accordance with the
                </p>
                <p class="texts">
                    User Agreement and Talent on Lease 's Privacy Policy.
                </p>
                <p class="texts">
                    4.2 As a condition of use, you promise not to use the Service for any purpose that is prohibited by these Terms of Use. You are responsible for all of your activity in connection with the Service. Talent on Lease has no obligation to monitor the Website, Content, or User Submissions.
                    However, Talent on Lease reserves the right to:
                </p>
                <p class="texts">
                     (i) remove, suspend, edit or modify any Content in its sole discretion, including without limitation any User Submissions at any time, without notice to you and for any reason (including, but not limited to, upon receipt of claims or allegations from third parties or authorities relating to such Content or if Talent on Lease is concerned that you may have violated these Terms of Use), or for no reason at all; and 
                </p>
                <p class="texts">
                     (ii) to remove, suspend or block any User Submissions from the Website. Talent on Lease also reserves the right to access, read, preserve, and disclose any information as Talent on Lease reasonably believes is necessary to: (i) satisfy any applicable law, regulation, legal process or governmental request; (ii) enforce these Terms of Use, including investigation of potential violations hereof;
                </p>
                <p class="texts">
                      (iii) detect, prevent, or otherwise address fraud, security or technical issues;
                </p>
                <p class="texts">
                    (iv) respond to user support requests; or 
                </p>
                <p class="texts">
                    (v) protect the rights, property or safety of Talent on Lease, its users and the public.
                </p>
                <p class="texts">
                    4.3 Users shall not use the Website in order to transmit, distribute, store or destroy material, including without limitation content provided by the Website:
                </p>
                <p class="texts">
                    (i) in a manner that will infringe the copyright, trademark, trade secret or other intellectual property rights of others or violate the privacy, publicity or other personal rights of others, or
                </p>
                <p class="texts">
                    (ii) collect any information about other Users including Members (including usernames and/or email addresses) for any purpose other than to solicit applications for project / assignment listings; or
                </p>
                <p class="texts">
                    (iii) modify, adapt, translate, or reverse engineer any portion of the Website and/or Services; or
                </p>
                <p class="texts">
                    (iv) that harasses or advocates harassment of another person.
                </p>
                <p class="texts">
                    4.4 Users are also prohibited from violating or attempting to violate the security of the Website, including, without limitation the following activities:
                </p>
                <p class="texts">
                     (i) accessing data not intended for such
                    User or logging into a server or account which the User is not authorized to access;
                </p>
                <p class="texts">
                     (ii) attempting to probe, scan or test the vulnerability of a system or network or to breach security or authentication measures without proper authorisation;
                </p>
                <p class="texts">
                      (iii) attempting to interfere with service to any User, host or network, including, without limitation, via means of submitting a virus to Website, overloading, “flooding”, “spamming”, “mailbombing” or “crashing”; or 
                </p>
                <p class="texts">
                      (iv)  forging any TCP/IP packet header or any part of the header information in any e-mail or newsgroup posting. Violations of system or network security may result in civil or criminal liability.
                </p>

                <h4>5. Remedies with the Website.</h4>
                <p class="texts">               
                    5.1 You understand and agree that the Website may review any content or project / assignment listing and in case the Website finds, in its sole discretion, that the User violates any terms of this Agreement especially Clause 4, the Website reserves the right to take actions to prevent/control such violation including without limitation, removing the offending communication or content from the Website and/or terminating the Membership of such violators and/or blocking their use of the Website and/or Service.
                </p>
                <p class="texts">
                    5.2 The Website shall also be entitled to investigate occurrences which may involve such
                    violations and take appropriate legal action, involve and cooperate with law enforcement
                    authorities in prosecuting Users who are involved in such violations.
                </p>
                <p class="texts">
                    5.3 In order to ensure a safe and effective experience for all the Users, the Website reserves the
                    right to limit the amount of data (including resume views) that may be accessed by them in
                    any given time period. These limits may be amended in the Website’s sole discretion from
                    time to time.
                </p>

                <h4> 6. Registration Process.</h4>
                <p class="texts">
                    6.1 No registration is required if you use the Website to browse. You may browse the Website
                    and view content without registering, but as a condition to using certain aspects of the
                    Website, you are required to register with Talent on Lease, by creating an account ("Account") on
                    the Website and represent, warrant and covenant that you provide us with accurate and
                    complete registration information (including, but not limited to a user name ("User Name"),
                    e-mail address and a password you will use to access the Website) and to keep your
                    registration information accurate and up-to-date. Failure to do so shall constitute a breach of
                    the User Agreement, which may result in immediate termination of your Account.
                </p>
                <p class="texts">
                    6.2 For the purpose of this User Agreement, Account means the account successfully opened by
                    the User on the Website by inserting information such as but not limited to name, contact
                    details, user name, and password as required to be filled in the registration process and
                    include any further changes and additions to the information from time to time.
                    You shall not:
                </p>
                <p class="texts">
                    i. create any Account for anyone other than yourself without such person's prior written
                    permission;
                </p>
                <p class="texts">
                    ii.use a User Name that is the name of another person with the intent to impersonate
                    that person;
                </p>
                <p class="texts">
                    iii. use a User Name or Account that is subject to any rights of a person other than you
                    without appropriate written authorization; or
                </p>
                <p class="texts">
                    iv. use a User Name that is a name that is otherwise offensive, vulgar or obscene or
                    otherwise unlawful.
                </p>
                <p class="texts">
                    6.3 Talent on Lease reserves the right to refuse registration of, or cancel a User Name in its sole
                    discretion. You are solely responsible and liable for all activity that occurs on your Account
                    and shall be responsible for maintaining the confidentiality of your Talent on Lease User Name and 
                    password. You shall never use another user's account without such other user's prior express
                    permission. You will immediately notify Talent on Lease in writing of any unauthorized use of your
                    Account, or other Account related security breach of which you are aware.
                </p>
                <p class="texts">
                    6.4 Any conduct by a User that in Talent on Lease 's exclusive discretion is in breach of the Terms of
                    Use or which restricts or inhibits any other User from using or enjoying the Services is strictly
                    prohibited. You represent that you are solely responsible for ensuring that these Terms of Use
                    are in compliance with all laws, rules and regulations applicable to you and the right to access
                    the Service is revoked where these Terms of Use or use of the Service is prohibited and, in
                    such circumstances, you agree not to use or access the Website or Services in any way.
                    The User to be entitled to avail the Services shall have to complete the registration
                    process (“Registration Process”) as provided below:
                </p>
                <p class="texts">
                    6.5 For Skill Providers
                    The Registration Process may involve only simple registration or may involve simple
                    registration and complete skill profile creation depending on the need of the Skill
                    Provider.
                    After simple registration, the Skill Provider needs to fill in the details in the complete
                    skill profile. The Skill Provider understands and agrees that the Website will screen
                    and verify the information provided by the Skill Provider in the complete skill profile
                    and at its sole discretion, increase the amount or number of information for the
                    Registration Process and may ask for further information even after Registration
                    Process. The Website may in its sole discretion, close the Account, if any information
                    provided is found to be false or the information provided is not sufficient. After the screening and verification of the information provided by the Skill Provider, the
                    Website may allow the Skill Provider apply for listings of projects / assignments.
                </p>
                <p class="texts">
                    6.6 For Opportunity-Providers:
                    The Registration Process may involve only simple registration. Simple registration is mandatory for the Opportunity-Provider and requires the Opportunity-Provider to provide certain basic information about itself such as company name, name of the authorised person and industry description and accordingly create an Account.
                    Simple registration entitles the Opportunity Providers to avail Services as provided in Clause 3.3 of this Agreement. After the simple registration is completed, the
                    Opportunity-Provider is required to fill listings of projects / assignments any time within the term of this Agreement by providing detailed information as requested in the Account for screening and verification and after such screening and verification the Opportunity Provider’s listings of projects / assignments will be listed. The Opportunity-Provider understands and agrees that the Website may screen and verify the information provided by the Opportunity-Provider and at its sole discretion, increase the amount or number of information for the Registration Process and may ask for further information even after Registration Process. The Website may in its sole discretion, close the Account, if any information provided is found to be false or the information provided is not sufficient.
                    Notwithstanding anything contained in clause 6.6 of this Agreement, the Website
                    may at its sole discretion, post the details of the Opportunity Provider, for any of its promotion scheme.
                </p>

                <h4> 7. Modification of Terms of Use. </h4>
                <p class="texts">   
                    You understand and agree that these Terms of Use, the Website and the Services can be modified by the Website at its sole discretion, at any time without prior notice, and shall be immediately effective. You agree to review the Terms of Use periodically so that You are aware of any such modifications and the Website shall not be liable for
                    any loss suffered by You on Your failure to review such modified Terms of Use.
                    Unless expressly stated otherwise, any new features, new services, enhancements or modifications to the Website or Service implemented after Your initial access of
                    Website or use of the Service shall be subject to these Terms of Use.
                </p>

                <h4>8. Maintenance.</h4>
                <p class="texts">  
                    The Website may at its sole discretion and without assigning any reason whatsoever at any time deactivate or/and suspend the User’s access to the Website and/or the Services (as the case may be) without giving any prior notice, to carry out system maintenance or/and upgrading or/and testing or/and repairs or/and other related work.
                    Without prejudice to any other provisions of this Agreement, the Website shall not be liable to indemnify the User for any loss or/and damage or/and costs or/and expense that the User may suffer or incur, and no fees or/and charges payable by the User to the Website shall be deducted or refunded or rebated, as a result of such deactivation or/and suspension.
                </p>

                <h4>9. Term and Termination.</h4>
                <p class="texts">              
                    9.1 These Terms of Use, with modifications as contemplated, shall remain in full force and effect during the use of the Website for all Users.
                </p>
                <p class="texts">
                    9.2 For Members, the Terms of Use shall commence from the start of the Registration Processas per Clause 6 of this Agreement and shall be valid till such time the account is not deleted by such Member from such commencement, unless terminated earlier by Talent on Lease or the User
                </p>
                <p class="texts">
                    9.3 These Terms of Use, with modifications as contemplated, shall remain in full force and effect during the use of the Website for all Users.
                </p>
                <p class="texts">
                     9.4 Talent on Lease may terminate your access to all or any part of the Service, with or without cause, with or without notice, effective immediately, which may result in the forfeiture and destruction of all information associated with your Account. If you wish to terminate your Account, you may do so by following the instructions on the Website. Any fees paid hereunder are non-refundable. All provisions of this Agreement which by their nature should survive termination shall survive termination, including, without limitation, ownership provisions, warranty disclaimers, indemnity and limitations of liability.
                 </p>
                 <p class="texts">
                    9.5 Notwithstanding anything contained in the Terms of Use, Clauses 5, 9, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22 and 23 shall survive any termination or expiration of these Terms of Use.
                </p>
               
          
            </div>
        </div>
    </div>
</div>


@endsection
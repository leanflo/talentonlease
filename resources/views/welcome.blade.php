<!doctype html>
<html lang="{{ app()->getLocale() }}">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="keywords" content="">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Fira+Sans|Roboto:300,400|Questrial|Satisfy">
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Fira+Sans|Roboto:300,400|Questrial|Satisfy">
    <link href="{{ asset('css/font-awesome.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/main.css') }}" rel="stylesheet">
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/animate.css') }}" rel="stylesheet">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
</head>

<body id="myPage" data-spy="scroll" data-target=".navbar" data-offset="60">
    <div class="header">
        <div class="bg-colors">
            @include('partials/header')
            
        <div class="">
        <div class="container">
          <div class="row">
            <div class="col-md-12 wow fadeIn delay-05s">
              <div class="banner-text text-center">
              <h2>TECH TALENT ON DEMAND</h2>
              <p>FULFIL YOUR URGENT IT REQUIREMENTS BY LEVERAGING RESOURCES OF OUR PARTNERS</p>
                
  <div class="col-md-6">

                <div style="margin-top:50px;">
                <p>Client</p>
                <a href="{{ route('create-post-requirement') }}" class="btn btn-primary btn-lg">POST ASSIGNMENT </a>
                <p>POST YOUR URGENT / CRITICAL IT REQUIREMENTS</p>
                </div>
            </div>
                            
  <div class="col-md-6">

                <div style="margin-top:50px;">
                <p>Partner</p>
                <a href="{{ route('post-requirement-list') }}" class="btn btn-primary btn-lg">FIND ASSIGNMENT</a>
                <p>FIND ASSIGNMENT FOR YOUR COMPANY</p>
                </div>
            </div>
            
            
            
            </div><!--row-->
            </div>
              <div class="overlay-detail text-center">
                <a href="#about"><i class="fa fa-angle-down"></i></a>
              </div>
            </div>
          </div>
        </div>
        </div>
    </div>
    </br>

<section class="work-process-sec text-center p-h-lg hidden-xs hidden-sm"">
        <div class="container-fluid">

  <div class="tab-content">
    <div role="tabpanel" class="tab-pane fade in active" id="home">
        

                <div class="col-xs-12">
                    <h2 class="section-titles">How it works ?</h2>
                </div>

                <div class="clearfix work-process-wrap col-md-6">
<h3 class="section-titles col-md-12 text-center">Client</h3>
                 <div class="work-process-blk">
                    <div class="work-process-icon">
                     <img src="img/01.png" alt="login & registration" class="img-responsive center-block">
                    </div> 
                    <h4 class="text-capitalize"><a href="{{ route('register') }}">login signup</a></h4> 
                  <!--   <p>
                      Build your profile on Security Lobby and enable registered recruiters to find your profile.
                    </p>   -->
                 </div><!-- work-process-blk -->

                 <div class="work-process-blk">
                    <div class="work-process-icon">
                     <img src="img/02.png" alt="post requirement" class="img-responsive center-block">
                    </div> 
                    <h4 class="text-capitalize"><a href="{{ route('create-post-requirement') }}">Post Assignment</a></h4> 
                   <!--  <p>
                      Save jobs and create job alerts for your interests to stay updated with new jobs.
                    </p> -->  
                 </div><!-- work-process-blk -->

                 <div class="work-process-blk">
                    <div class="work-process-icon">
                     <img src="img/03.png" alt="find talent" class="img-responsive center-block">
                    </div> 
                    <h4 class="text-capitalize">find talent</h4> 
                   <!--  <p>
                        Upload your resumes and cover letters to get quickly noticed by recruiters in Security Lobby.
                    </p>  --> 
                 </div><!-- work-process-blk -->

                 <div class="work-process-blk">
                    <div class="work-process-icon">
                     <img src="img/04.png" alt="sign contract" class="img-responsive center-block">
                    </div> 
                    <h4 class="text-capitalize">sign contract</h4> 
                    <!-- <p>
                       Get discovered by recruiters by making your profile searchable by the recruiters.
                    </p>  --> 
                 </div><!-- work-process-blk -->

                  <div class="work-process-blk">
                    <div class="work-process-icon">
                     <img src="img/05.png" alt="execute assignment" class="img-responsive center-block">
                    </div> 
                    <h4 class="text-capitalize">execute assignment</h4> 
                    <!-- <p>
                       Get discovered by recruiters by making your profile searchable by the recruiters.
                    </p>  --> 
                 </div><!-- work-process-blk -->

                </div><!-- work-process-wrap -->
           

                <div class="clearfix work-process-wrap col-md-6 bk">
<h3 class="section-titles col-md-12 text-center">Partner</h3>
                 <div class="work-process-blk">
                    <div class="work-process-icon">
                     <img src="img/01.png" alt="login registration" class="img-responsive center-block">
                    </div> 
                    <h4 class="text-capitalize"><a href="{{ route('register') }}">login signup</a></h4> 
                  <!--   <p>
                      Build your profile on Security Lobby and enable registered recruiters to find your profile.
                    </p>   -->
                 </div><!-- work-process-blk -->

                 <div class="work-process-blk">
                    <div class="work-process-icon">
                     <img src="img/07.png" alt="Share profile" class="img-responsive center-block">
                    </div> 
                    <h4 class="text-capitalize">Share profile</h4> 
                   <!--  <p>
                      Save jobs and create job alerts for your interests to stay updated with new jobs.
                    </p> -->  
                 </div><!-- work-process-blk -->

                 <div class="work-process-blk">
                    <div class="work-process-icon">
                     <img src="img/08.png" alt="Search Projects" class="img-responsive center-block">
                    </div> 
                    <h4 class="text-capitalize">search projects</h4> 
                   <!--  <p>
                        Upload your resumes and cover letters to get quickly noticed by recruiters in Security Lobby.
                    </p>  --> 
                 </div><!-- work-process-blk -->

                 <div class="work-process-blk">
                    <div class="work-process-icon">
                     <img src="img/04.png" alt="sign contract" class="img-responsive center-block">
                    </div> 
                    <h4 class="text-capitalize">sign contract</h4> 
                    <!-- <p>
                       Get discovered by recruiters by making your profile searchable by the recruiters.
                    </p>  --> 
                 </div><!-- work-process-blk -->

                  <div class="work-process-blk">
                    <div class="work-process-icon">
                     <img src="img/05.png" alt="execute assignment" class="img-responsive center-block">
                    </div> 
                    <h4 class="text-capitalize">execute assignment</h4> 
                    <!-- <p>
                       Get discovered by recruiters by making your profile searchable by the recruiters.
                    </p>  --> 
                 </div><!-- work-process-blk -->


                </div><!-- work-process-wrap -->

    </div>


     <div role="tabpanel" class="tab-pane fade" id="profile">
        

                <div class="col-xs-12">
                    <h2 class="section-title">How it works</h2>
                </div>
                <div class="clearfix work-process-wrap">

                 <div class="work-process-blk col-xs-12 col-sm-3">
                    <div class="work-process-icon">
                     <img src="images/create-profile.svg" alt="Security Create Profile" class="img-responsive center-block">
                    </div> 
                    <h4 class="text-capitalize">Build a profile</h4> 
                    <p>
                      Build your profile on Security Lobby and enable registered recruiters to find your profile.
                    </p>  
                 </div><!-- work-process-blk -->

                 <div class="work-process-blk col-xs-12 col-sm-3">
                    <div class="work-process-icon">
                     <img src="images/job-alert.svg" alt="Security Job Alert" class="img-responsive center-block">
                    </div> 
                    <h4 class="text-capitalize">Be the first to be alerted</h4> 
                    <p>
                      Save jobs and create job alerts for your interests to stay updated with new jobs.
                    </p>  
                 </div><!-- work-process-blk -->

                 <div class="work-process-blk col-xs-12 col-sm-3">
                    <div class="work-process-icon">
                     <img src="images/resume-cover-letter.svg" alt="Security Resume Cover" class="img-responsive center-block">
                    </div> 
                    <h4 class="text-capitalize">Upload your Resumes</h4> 
                    <p>
                        Upload your resumes and cover letters to get quickly noticed by recruiters in Security Lobby.
                    </p>  
                 </div><!-- work-process-blk -->

                 <div class="work-process-blk col-xs-12 col-sm-3">
                    <div class="work-process-icon">
                     <img src="images/view-profile.svg" alt="Security View Profile" class="img-responsive center-block">
                    </div> 
                    <h4 class="text-capitalize">Make your profile searchable</h4> 
                    <p>
                       Get discovered by recruiters by making your profile searchable by the recruiters.
                    </p>  
                 </div><!-- work-process-blk -->
                </div><!-- work-process-wrap -->

            <div class="col-xs-12 text-center m-t-sm">
             <a href="http://securitylobby.com/register" class="btn-blue-outline">Get Started&nbsp;&nbsp;<span class="fa fa-paper-plane-o"></span></a>
            </div>  


            

    </div>
  
  </div>








            <!-- </div> -->
        </div><!-- container -->
    </section>
  
  <section id="abouts" class="section-padding wow fadeIn delay-05s">
    <div class="container-fluid">
      <div class="row">
     <div class="col-md-12 text-center"><h2 style="color:white;"><u>Client</u></h2> 
</div>
        <div class="col-md-7 text-center">
          <h2 class="title-text text-uppercase">
            Find resources for short term</br> technology requirements
          </h2>
<div class="right-sec">       
<p>•    Create your profile on the platform</p>
<p>•    SHare details of the project and skill set required</p>
<p>•    Find resources from the database of resources</p>
<p>•    Assess skills</p>
<p>•    Sign a contract and start engagement</p>
 </div>
        </div>
        <div class="col-md-4 text-left">
          <div class="about-text">
            <h2>Post your Assignment</h2>
       <div style="margin-top:50px;"><a href="{{ url('/create-post-requirement') }}" class="btn btn-primary btn-lg" style="color: #232370;font-weight: 600;text-transform: uppercase;border-color: #ffffff;background-color: #ffffff;">get started</a></div>
          </div>
        </div>
      </div>
    </div>
  </section>
<br>


  <section id="about" class="section-padding wow fadeIn delay-05s">
    <div class="container-fluid">
      <div class="row">
     <div class="col-md-12 text-center"><h2 style="color:white;"><u>Partner</u></h2> 
</div>
        <div class="col-md-4 text-left2">
          <div class="about-text">
            <h2>Find Assignment</h2>
       <div style="margin-top: 50px;"><a href="{{ route('post-requirement-list') }}" class="btn btn-primary btn-lg"style="color: #232370;font-weight: 600;text-transform: uppercase;border-color: #ffffff;background-color: #ffffff;">get started</a></div>
          </div>
        </div>    
        <div class="col-md-7 text-center">
          <h2 class="title-text text-uppercase">
           Find Assignment </br>for your bench staff
          </h2>
<div class="left-sec">        
<p>•    Create your profile on the platform</p>
<p>•    SHare profiles of bench staff</p>
<p>•    Search projects for the bench staff</p>
<p>•    Get connected to the client</p>
<p>•    Sign a contract and start engagement</p>
 </div>
        
        </div>

      </div>
    </div>
  </section>
<br>

<section class="media text-center bg-lt-grey hidden-xs hidden-sm"">
        <div class="container">
            <div class="row">
                <div class="col-xs-12"style="margin-bottom: 50px;">
                    <h2 class="section-title">Our Clients</h2>
                </div>
                
  <div class="col-md-12 col-sm-12 col-xs-12">
                <div id="Carousel" class="carousel slide">
                 
                <ol class="carousel-indicators">
                    <li data-target="#Carousel" data-slide-to="0" class="active"></li>
                    <li data-target="#Carousel" data-slide-to="1"></li>
                    <li data-target="#Carousel" data-slide-to="2"></li>
                </ol>
                 
                <!-- Carousel items -->
                <div class="carousel-inner">
                    
                <div class="item active">
                 <div class="row">
                   <div class="col-md-2 col-xs-4"><a href="" class="thumbnail"><img src="img/microsoft.jpg" alt="Image" style="height:80px;"></a></div>
                   <div class="col-md-2 col-xs-4"><a href="" class="thumbnail"><img src="img/lg.jpg" alt="Image" style="height:80px;"></a></div>
                   <div class="col-md-2 col-xs-4"><a href="" class="thumbnail"><img src="img/baxter.jpg" alt="Image" style="height:80px;"></a></div>
                   <div class="col-md-2 col-xs-4"><a href="" class="thumbnail"><img src="img/atten.jpg" alt="Image" style="height:80px;"></a></div>
                   <div class="col-md-2 col-xs-4"><a href="" class="thumbnail"><img src="img/cocacola.jpg" alt="Image" style="height:80px;"></a></div>
                   <div class="col-md-2 col-xs-4"><a href="" class="thumbnail"><img src="img/hcl.jpg" alt="Image" style="height:80px;"></a></div>
                 </div><!--.row-->
                </div><!--.item-->
                 
                <div class="item">
                 <div class="row">
                   <div class="col-md-2 col-xs-4"><a href="" class="thumbnail"><img src="img/triland.jpg" alt="Image" style="max-height:80px;"></a></div>
                   <div class="col-md-2 col-xs-4"><a href="" class="thumbnail"><img src="img/appollo.jpg" alt="Image" style="max-height:80px;"></a></div>
                   <div class="col-md-2 col-xs-4"><a href="" class="thumbnail"><img src="img/kpmg.jpg" alt="Image" style="max-height:80px;"></a></div>
                   <div class="col-md-2 col-xs-4"><a href="" class="thumbnail"><img src="img/icici-bank.jpg" alt="Image" style="max-height:80px;"></a></div>
                   <div class="col-md-2 col-xs-4"><a href="" class="thumbnail"><img src="img/educomp.jpg" alt="Image" style="max-height:80px;"></a></div>
                   <div class="col-md-2 col-xs-4"><a href="" class="thumbnail"><img src="img/federal-mogul.jpg" alt="Image" style="max-height:80px;"></a></div>
                 </div><!--.row-->
                </div><!--.item-->
                 
                <div class="item">
                 <div class="row">
                   <div class="col-md-2 col-xs-4"><a href="" class="thumbnail"><img src="img/pld.jpg" alt="Image" style="max-height:80px;"></a></div>
                   <div class="col-md-2 col-xs-4"><a href="" class="thumbnail"><img src="img/iift.jpg" alt="Image" style="max-height:80px;"></a></div>
                   <div class="col-md-2 col-xs-4"><a href="" class="thumbnail"><img src="img/ITC.jpg" alt="Image" style="max-height:80px;"></a></div>
                   <div class="col-md-2 col-xs-4"><a href="" class="thumbnail"><img src="img/jetsave.jpg" alt="Image" style="max-height:80px;"></a></div>
                   <div class="col-md-2 col-xs-4"><a href="" class="thumbnail"><img src="img/hcl.jpg" alt="Image" style="max-height:80px;"></a></div>
                   <div class="col-md-2 col-xs-4"><a href="" class="thumbnail"><img src="img/lg.jpg" alt="Image" style="height:80px;"></a></div>
                 </div>
                </div>
                 

                 
  </div>


        </div>
    </section>
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<script>
$(document).ready(function() {
    $('#Carousel').carousel({
        interval: 3000
    })
});
</script>

  <section class="section-padding wow fadeIn delay-05s count">
    <div class="container">
      <div class="row">
        <div class="col-md-4 col-xs-12 bb" >1000+  </br>technology<br> professional
  </div>
        <div class="col-md-4 col-xs-12 "style="padding: 25px;">100+  </br>clients</br>
  </div> 
        <div class="col-md-4 col-xs-12 bc">300+ </br> project <br>Assignments
   </div>
</div> </div>
</section>

    <br>

    @include('partials/footer')

</body>

</html>
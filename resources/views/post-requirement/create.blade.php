@extends('layouts.app') @section('content')

<form method="POST" action="{{ route('create-post-requirement') }}" enctype="multipart/form-data">
    @csrf
    <div class="">
        <div class="col-md-offset-1 col-md-10">
            <div class="row form-group">
                <div class="col-md-12">
                    <input class="col-md-12 col-xs-12 borderblueall wholepadding bluecolor req_field"name="user_id" value="{{ Auth::id() }}" type="hidden">
                </div>
            </div>
           
            <div class="row form-group">
                 <div class="col-md-4">
                    <label for="title" class="text-uppercase">Title/Role <span style="color:red;">*</span></label>
                    <input type="text" class="form-control{{ $errors->has('title') ? ' is-invalid' : '' }}" name="title" value="{{ old('title') }}" placeholder="Enter title" autofocus>
                    @if ($errors->has('title'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('title') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="col-md-4">
                    <label for="applicant_type" class="text-uppercase">Who Can Apply? <span style="color:red;">*</span></label>
                    <select class="form-control{{ $errors->has('applicant_type') ? ' is-invalid' : '' }}" name="applicant_type" id="applicant_type" name="applicant_type">
                        <option value="">Select One</option>
                        <option value="Only Individual Candidates" {{(old('applicant_type') == 'Only Individual Candidates' ? 'selected':'')}}>Only Individual Candidates</option>
                        <option value="Only Small/Medium Enterprises" {{(old('applicant_type') == 'Only Small/Medium Enterprises' ? 'selected':'')}}>Only Small/Medium Enterprises</option>
                        <option value="Individual Candidates and Small/Medium Enterprises" {{(old('applicant_type') == 'Individual Candidates and Small/Medium Enterprises' ? 'selected':'')}}>Individual Candidates and Small/Medium Enterprises</option>
                    </select>
                    @if ($errors->has('applicant_type'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('applicant_type') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="col-md-4">
                    <label for="num_of_position" class="text-uppercase">No. of Positions <span style="color:red;">*</span></label>
                    <input class="form-control{{ $errors->has('num_of_position') ? ' is-invalid' : '' }}" name="num_of_position" value="{{ old('num_of_position') ? old('num_of_position') : 1 }}" id="num_of_position" min="1" type="number">
                    @if ($errors->has('num_of_position'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('num_of_position') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
            <div class="row form-group">
                <!-- /.row -->
                <div class="col-md-6">
                    <label for="category" class="text-uppercase">Broad Area <span style="color:red;">*</span></label>
                    <select class="form-control{{ $errors->has('category') ? ' is-invalid' : '' }}" name="category" id="category">
                        <option value="">Select Category</option>
                        @foreach ($categories as $category)
                          <option value="{{ $category->id }}" {{(old('category') == $category->id?'selected':'')}}>{{ $category->name}}</option>
                        @endforeach
                    </select>
                    @if ($errors->has('category'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('category') }}</strong>
                        </span>
                    @endif

                </div>
                <div class="col-md-6">
                    <label for="sub_category" class="text-uppercase">Sub-Category<span style="color:red;">*</span></label>
                    <select multiple="multiple" class="form-control{{ $errors->has('sub_category') ? ' is-invalid' : '' }}" name="sub_category[]" id="sub_category">
                        
                        @foreach ($sub_categories as $sub_category)
                          <option value="{{ $sub_category->id }}" {{ (collect(old('sub_category'))->contains($sub_category->id)) ? 'selected':'' }}>{{ $sub_category->name}}</option>
                        @endforeach
                    </select>
                    @if ($errors->has('sub_category'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('sub_category') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
            <div class="row form-group">
                <!-- /.row -->
                <div class="col-md-4">
                    <label for="industry" class="text-uppercase"><strong>Industry</strong> <span style="color:red;">*</span></label>

                    <select class="form-control req_field" id="industry" name="industry">
                        <option value="">Select Industry</option>
                        @foreach ($industries as $industry)
                          <option value="{{ $industry->id }}" {{(old('industry') == $industry->id?'selected':'')}}>{{ $industry->name}}</option>
                        @endforeach
                    </select>
                    @if ($errors->has('industry'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('industry') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="col-md-4">
                    <label for="exp_required" class="text-uppercase"><strong>Minimum Work Experience Required</strong> </label>
                    <select class="form-control" id="exp_required" name="exp_required">
                        <option value="">Select Work Experience</option>
                        <option value="less than 1" {{(old('exp_required') == 'less than 1' ? 'selected':'')}}>Less than 1</option>
                        @for ($i = 1; $i <= 30; $i++)
                            <option value="{{ $i }}" {{(old('exp_required') == $i ? 'selected':'')}}>{{ $i }}</option>
                        @endfor  
                        <!-- <option value="0">Less than 1</option>
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5">5</option>
                        <option value="6">6</option>
                        <option value="7">7</option>
                        <option value="8">8</option>
                        <option value="9">9</option>
                        <option value="10">10</option>
                        <option value="11">11</option>
                        <option value="12">12</option>
                        <option value="13">13</option>
                        <option value="14">14</option>
                        <option value="15">15</option>
                        <option value="16">16</option>
                        <option value="17">17</option>
                        <option value="18">18</option>
                        <option value="19">19</option>
                        <option value="20">20</option>
                        <option value="21">21</option>
                        <option value="22">22</option>
                        <option value="23">23</option>
                        <option value="24">24</option>
                        <option value="25">25</option>
                        <option value="26">26</option>
                        <option value="27">27</option>
                        <option value="28">28</option>
                        <option value="29">29</option>
                        <option value="31">30+</option> -->
                    </select>
                    @if ($errors->has('exp_required'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('exp_required') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="col-md-4">
                    <label for="qualification" class="text-uppercase"><strong>Qualification</strong> <span style="color:red;">*</span></label>
                    <select class="form-control req_field" id="qualification" name="qualification">
                        <option value="" selected="selected">Select Qualification</option>
                        <option value="Graduate" {{(old('qualification') == 'Graduate' ? 'selected':'')}}>Graduate</option>
                        <option value="Post Graduate or Higher" {{(old('qualification') == 'Post Graduate or Higher' ? 'selected':'')}}>Post Graduate or Higher</option>
                    </select>
                    @if ($errors->has('qualification'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('qualification') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
            <div class="row form-group">
                <!-- /.row -->
                <div class="col-md-6">
                    <label for="assignment_detail" class="text-uppercase"><strong> Assignment Details</strong> <span style="color:red;">*</span></label>
                    <textarea class="form-control col-md-12 expand expand {{ $errors->has('assignment_detail') ? ' is-invalid' : '' }}" name="assignment_detail" cols="40" id="assignment_detail" placeholder="Add details describing the role and help potential applicants learn what makes this a great opportunity." rows="2" style="height: 148px; overflow: hidden; padding-top: 0px; padding-bottom: 0px; z-index: auto; position: relative; line-height: 20px; font-size: 14px; transition: none; background: transparent !important;" spellcheck="true">{{ old('assignment_detail') }}</textarea>
                    <grammarly-btn>
                        <div class="_1BN1N Kzi1t MoE_1 _2DJZN" style="z-index: 2; transform: translate(354.8px, 144px);">
                            <div class="_1HjH7">
                                <div title="Protected by Grammarly" class="_3qe6h">&nbsp;</div>
                            </div>
                        </div>
                    </grammarly-btn>
                    @if ($errors->has('assignment_detail'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('assignment_detail') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="col-md-6">
                    <label for="skill_required" class="text-uppercase"><strong>Skills Required</strong> </label>
                    <textarea class="form-control req_field col-md-12 expand {{ $errors->has('skill_required') ? ' is-invalid' : '' }}" name="skill_required" cols="40" id="skill_required" placeholder="4- 6 bullets highlighting skills required for this role" rows="2" style="height: 148px; overflow: hidden; padding-top: 0px; padding-bottom: 0px; z-index: auto; position: relative; line-height: 20px; font-size: 14px; transition: none; background: none 0% 0% / auto repeat scroll padding-box border-box rgb(255, 255, 255);" spellcheck="true">{{ old('skill_required') }}</textarea>
                   
                </div>
            </div>

            <div class="row form-group">
                <!-- /.row -->
                <div class="col-md-6">
                    <label for="assignment_duration" class="text-uppercase">Assignment Duration </label><span class="blue">&nbsp;(In months)</span><span style="color:red;">*</span>
                    <select class="form-control req_field" id="assignment_duration" name="assignment_duration">
                        <option value="">Select Assignment Duration</option>
                        <option value="less than 1" {{(old('assignment_duration') == 'less than 1' ? 'selected':'')}}>Less than 1</option>
                        @for ($i = 1; $i <= 12; $i++)
                            <option value="{{ $i }}" {{(old('assignment_duration') == $i ? 'selected':'')}}>{{ $i }}</option>
                        @endfor    
                        <!-- <option value="LESSTHANYEAR">less than 1</option>
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option> 
                        <option value="4">4</option>
                        <option value="5">5</option>
                        <option value="6">6</option>
                        <option value="7">7</option>
                        <option value="8">8</option>
                        <option value="9">9</option>
                        <option value="10">10</option>
                        <option value="11">11</option>
                        <option value="12">12</option>
                        <option value="MORETHAN12">&gt;12 (Part-time ongoing)</option> -->
                    </select>
                    @if ($errors->has('assignment_duration'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('assignment_duration') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="col-md-6">
                    <label for="talent_required" class="text-uppercase">Capacity Required <span style="color:red;">*</span></label>
                    <select class="form-control req_field" id="talent_required" name="talent_required">
                        <option value="" selected="selected">Select Capacity</option>
                        <option value="Full Time" {{(old('talent_required') == 'Full Time' ? 'selected':'')}}>Full Time</option>
                        <option value="20+ Hrs/Week" {{(old('talent_required') == '20+ Hrs/Week' ? 'selected':'')}}>20+ Hrs/Week</option>
                        <option value="15-20 Hrs/Week" {{(old('talent_required') == '15-20 Hrs/Week' ? 'selected':'')}}>15-20 Hrs/Week</option>
                        <option value="10-15 Hrs/Week" {{(old('talent_required') == '10-15 Hrs/Week' ? 'selected':'')}}>10-15 Hrs/Week</option>
                        <option value="8-10 Hrs/Week" {{(old('talent_required') == '8-10 Hrs/Week' ? 'selected':'')}}>8-10 Hrs/Week</option>
                    </select>
                    @if ($errors->has('talent_required'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('talent_required') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
            <div class="row form-group nobottommargin">
                <!-- /.row -->
                <div class="col-md-4">
                    <label for="work_nature" class="text-uppercase">Work Nature <span style="color:red;">*</span></label>
                    <select class="form-control req_field" id="work_nature" name="work_nature">
                        <option value="" selected="selected">Select Work Nature</option>
                        <option value="On-Site" {{(old('work_nature') == 'On-Site' ? 'selected':'')}}>On-Site</option>
                        <option value="Remote" {{(old('work_nature') == 'Remote' ? 'selected':'')}}>Remote</option>
                        <option value="Both (On-Site and Remote)" {{(old('work_nature') == 'Both (On-Site and Remote)' ? 'selected':'')}}>Both (On-Site and Remote)</option>
                    </select>
                    @if ($errors->has('work_nature'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('work_nature') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="col-md-4">
                    <label for="location" class="text-uppercase">Location <span style="color:red;">*</span></label>
                    <select class="form-control input-md" id="location" name="location[]" multiple="multiple">
                        @foreach ($cities as $city)
                          <option value="{{ $city->id }}" {{ (collect(old('location'))->contains($city->id)) ? 'selected':'' }}>{{ $city->name}}</option>
                        @endforeach
                    </select>
                    @if ($errors->has('location'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('location') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="col-md-4">
                    <label for="project_start_date" class="text-uppercase">Project starts on <span style="color:red;">*</span></label>
                    <input type="date" class="form-control" id="project_start_date" name="project_start_date" value="{{ old('project_start_date') }}">
                    @if ($errors->has('project_start_date'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('project_start_date') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="col-md-4 margintop">
                    <label for="project_last_date" class="text-uppercase">Cut-off date for applications </label>
                    <input type="date" class="form-control" id="project_last_date" name="project_last_date" value="{{ old('project_last_date') }}">
                    @if ($errors->has('project_last_date'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('project_last_date') }}</strong>
                        </span>
                    @endif
                </div>

            </div>
           
            <div class="row form-group">
                <div class="col-md-12">
                    <h4><span class="blue font">Project Budget</span> <span class="blue">(Optional)</span> </h4>
                </div>
                <div class="col-md-3">
                    <label for="budget_type" class="text-uppercase">Budget Type </label>
                    <select class="form-control" id="budget_type" name="budget_type">
                        <option value="">Select One</option>
                        <option value="Total Budget" {{(old('budget_type') == 'Total Budget' ? 'selected':'')}}>Total Budget</option>
                        <option value="Daily" {{(old('budget_type') == 'Daily' ? 'selected':'')}}>Daily</option>
                        <option value="Monthly" {{(old('budget_type') == 'Monthly' ? 'selected':'')}}>Monthly</option>
                    </select>
                </div>
                
                <div class="col-md-3 ">
                    <div class="col-md-10 col-xs-10  noleftpadding norightpadding">
                        <label for="budget_amount" class="text-uppercase ">ESTIMATED BUDGET</label>
                    </div>
                    <div class="col-md-5 col-xs-12  noleftpadding norightpadding">
                        <input class="form-control" id="min_budget" name="min_budget" placeholder="Min" type="text" value="{{ old('min_budget') }}">
                    </div>
                    <div class="col-md-1 col-xs-12" style="margin:4px 4px 0px 0px; !important;"><strong>-</strong></div>
                    <div class="col-md-5 col-xs-12  noleftpadding norightpadding">
                        <input class="form-control" id="max_budget" name="max_budget" placeholder="Max" type="text" value="{{ old('max_budget') }}">
                    </div>
                </div>
                
                <div class="col-md-6">
                      <label for="budget_type" class="text-uppercase">Assignment Description<span class="blue">(Optional)</span> </label>
                </div>
                <div class="col-md-6">
                    <!-- <p>Please upload the document in doc, docx, or pdf format</p> -->
                    <input type="file" class="btn btn-light-blue" id="assignment_doc" name="assignment_doc" style="border:1px solid !important;">
                     @if ($errors->has('assignment_doc'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('assignment_doc') }}</strong>
                        </span>
                    @endif
                </div>

               
            </div>

            <div class=" form-group">
                <div class="col-md-12 nopadding text-center">                
                    <input type="submit" class="btn btn-light-blue novecento-normal post_btn_save" title="Submit your details" value="Post" style="color: #ffffff; background-color: #232370;">

                </div>
            </div>
        </div>
    </div>

    </div>
    </div>
    </div>
</form>

@endsection
  @extends('layouts.app') @section('content')
<style>
    .dropdown-item{padding: 50px;
    color: #232370;
    font-weight: 600;}
</style>
  <section  class="section-padding wow fadeIn delay-05s">	  
    <div class="container">
        <div class="table-wrapper">
            <div class="table-title">
                <div class="row">
                    <div class="col-sm-8"><h2>Post <b>Details</b></h2></div>
                    <div class="col-sm-4">
                        <!-- <div class="search-box">
                            <i class="material-icons">&#xE8B6;</i>
                            <input type="text" class="form-control" placeholder="Search&hellip;">
                        </div> -->
                    </div>
                </div>
            </div>
            @if(count($posts) > 0)
            <table class="table table-striped table-hover table-bordered">
                <thead>
                    <tr>
                        <th>S.No</th>
                        <th>TITLE</th>
                        <th>BROAD AREA</th>
                        <th>SUB-CATEGORY</th>
                        <th>NO. OF POSITIONS</th>
                        <th>QUALIFICATION</th>
                        <th>EXPERIENCE REQUIRED</th>
                        <th>WORK NATURE</th>
                        <th>LOCATION</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($posts as $key => $post)
                    <tr>
                        <td>{{ $no++ }}</td>
                        <td>{{ $post->title }}</td>
                        <td>{{ $post->category }}</td>
                        <td>{{ $post->sub_category }}</td>
                        <td>{{ $post->num_of_position }}</td>
                        <td>{{ $post->qualification }}</td>
                        <td>{{ $post->exp_required }}</td>
                        <td>{{ $post->work_nature }}</td>
                        <td>{{ $post->location }}</td>
                    </tr>
                   @endforeach
                </tbody>
            </table>
            <!-- <div class="clearfix">
                <div class="hint-text">Showing <b>5</b> out of <b>25</b> entries</div>
                <ul class="pagination">
                    <li class="page-item disabled"><a href="#"><i class="fa fa-angle-double-left"></i></a></li>
                    <li class="page-item"><a href="#" class="page-link">1</a></li>
                    <li class="page-item"><a href="#" class="page-link">2</a></li>
                    <li class="page-item active"><a href="#" class="page-link">3</a></li>
                    <li class="page-item"><a href="#" class="page-link">4</a></li>
                    <li class="page-item"><a href="#" class="page-link">5</a></li>
                    <li class="page-item"><a href="#" class="page-link"><i class="fa fa-angle-double-right"></i></a></li>
                </ul>
            </div> -->
        </div>
         @else
            <div class="list-photo-rowpg alert alert-warning">{{ trans('No post found') }}</div>
        @endif  
    </div>  
</section>	

@endsection
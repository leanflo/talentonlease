<div>
    <h3>Hi,</h3> 
	<p>Dear Talent on Lease.</p>
	<p>You have received an application for contact.</p>
	<p><strong>Name: </strong> {{ $user['firstname']." ".$user['lastname'] }}</p>
	<p><strong>Email: </strong> {{ $user['email'] }}</p>
	<p><strong>Phone: </strong> {{ $user['phone'] }}</p>
	<p><strong>Message: </strong> {{ $user['message'] }}</p>
	<p>Thanks</p>
	<p><strong>{{ $user['firstname']." ".$user['lastname'] }}</strong></p>
</div>
<!DOCTYPE html>
<html>
<head>
    <title>Welcome Email</title>
</head>
 
<body>
<h4>Dear {{ $user->first_name." ".$user->last_name }}</h4>
<p>Welcome to Talent On Lease! Your Registered email ID on the TOL Platform is {{ $user->email }}. <br/>Please click on the verification link to verify your account:</p>
<br/>
<a href="{{url('user/verify', $user->verifyUser->token)}}">Verify Email</a>
<br/>
<p>Thanks,</p>
<p><b>Team Talent on Lease.</b></p>
</body>
 
</html>
<header id="main-header">
  <nav class="navbar navbar-default navbar-fixed-top">
    <div class="container">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#lauraMenu">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a href="{{ url('/') }}"><img style="padding:0;" class="navbar-brand" src="{{ asset('img/logo.png') }}" /></a>
      </div>
      <div class="collapse navbar-collapse" id="lauraMenu">
        <ul class="nav navbar-nav navbar-right navbar-border">
          <li class="active"><a href="{{ url('/') }}">Home</a></li>
          <li><a href="{{ url('/about') }}">About</a></li>
          <!-- <li><a href="#">Portfolio</a></li>
          <li><a href="#">Testimonial</a></li> -->
          <!-- <li><a href="{{ url('/contact-us') }}">Contact Us</a></li> -->
          

            @guest
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('register') }}">{{ __('Signup') }}</a>
                </li>
            @else

                @if(Auth::user()->role_id == 1)
                  <li><a href="{{ url('/post-requirement-list') }}">Assignment List</a></li>
                @elseIf(Auth::user()->role_id == 2)
                  <li><a href="{{ url('/create-post-requirement') }}">Post Assignment</a></li>
                @elseIf(Auth::user()->role_id == 3)
                  <li><a href="{{ url('/create-project') }}">Profile</a></li>
                @endIf
            
                <li class="nav-item dropdown">
                    <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                        {{ Auth::user()->first_name }} <span class="caret"></span>
                    </a>

                    <div class="dropdown-menu dropdown-menu-right menudrp" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="{{ route('logout') }}"
                           onclick="event.preventDefault();
                                         document.getElementById('logout-form').submit();"><i class="fa fa-power-off"></i>
                            {{ __('Logout') }}
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </div>
                </li>
            @endguest

        </ul>
      </div>
    </div>
  </nav>
</header>
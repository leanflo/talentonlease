<section class="footer">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-4 foot-link service">
                <h4 class="foot-title">Company</h4>
                <ul>
                    <li><a href="{{ url('/about') }}">About</a></li>
                    <!-- <li><a href="#">portfolio</a></li>
                    <li><a href="{{ url('/contact-us') }}">contact us</a></li> -->
                </ul>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4 foot-link service">
                <h4 class="foot-title">Other Links</h4>
                <ul>
                    <li><a href="{{ url('/term-of-use') }}">Terms of Use</a></li>
                    <li><a href="{{ url('/privacy-policy') }}">Privacy Policy</a></li>
                </ul>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4 foot-link foot-con">
                <h4 class="foot-title">Contact</h4>
                <ul>
                    <li>
                        <p><span class="fa fa-map-marker"></span>C-58, Sector 63,Noida 201301 (UP)</p>
                    </li>
                    <li><a href="mailto:info@talentonlease.com"><span class="fa fa-envelope-o"></span>info@talentonlease.com</a></li>
                    <li><a href="tel:+919899240555"><span class="fa fa-phone"></span>+91 98992 40555</a></li>

                </ul>
                <div class="social-icon">
                    <ul class="no-margin">
                        <li>
                            <a href="https://www.facebook.com/Talent-on-Lease-2181624008775260" target="_blank"><span class="fa fa-facebook"></span></a>
                        </li>
                        <li>
                            <a href="https://www.instagram.com/talentonlease/" target="_blank"><span class="fa fa-instagram"></span></a>
                        </li>
                        <li>
                            <a href="https://www.linkedin.com/company/talent-on-lease/" target="_blank"><span class="fa fa-linkedin"></span></a>
                        </li>
                        <!-- <li>
                            <a href="" target="_blank"><span class="fa fa-google"></span></a>
                        </li> -->
                    </ul>
                </div>
            </div>
        </div>
        <!-- row end -->
    </div>
    <!-- .container end -->
</section>
<section class="foot-blk">
    <div class="container">
        <div class="row">
            <!-- <div class="col-xs-12"> -->
            <div class="col-md-6 col-xs-12">
                <p class="copyright">© 2018 Talent on Lease | All Rights Reserved</p>
            </div>
            <div class="col-md-6 col-xs-12">
                <p class="design-dev">designed &amp; Developed By  <a href="https://leanflo.co/" target="_blank" style="color: #ccc;">Leanflo Inc</a></p>
            </div>
            <!-- </div> -->
        </div>
    </div>
</section>

<script src="{{ asset('js/jquery.min.js') }}"></script>
<script src="{{ asset('js/jquery.easing.min.js') }}"></script>
<script src="{{ asset('js/bootstrap.min.js') }}"></script>
<script src="{{ asset('js/jquery.bxslider.min.js') }}"></script>
<script src="{{ asset('js/wow.js') }}"></script>
<script src="{{ asset('js/custom.js') }}"></script>
<script src="{{ asset('js/picker.js') }}"></script>
<script src="{{ asset('js/example.js') }}"></script>
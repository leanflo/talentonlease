@extends('layouts.app') @section('content')

<form method="POST" action="{{ route('create-project') }}" enctype="multipart/form-data">
    @csrf
    <div class="">
        <div class="col-md-offset-1 col-md-10 ">
            <div class="row form-group">
                <div class="col-md-12">
                    <input class="col-md-12 col-xs-12 borderblueall wholepadding bluecolor req_field"name="user_id" value="{{ Auth::id() }}" type="hidden">
                </div>
            </div>

            <div class="row form-group">
                <div class="col-md-12">
                    <label for="job_title" class="text-uppercase">Job Tittle<span style="color:red;">*</span></label>
                    <input type="text" id="job_title" name="job_title" class="form-control req_field {{ $errors->has('job_title') ? ' is-invalid' : '' }}" value="{{ old('job_title') }}" autofocus>

                    @if ($errors->has('job_title'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('job_title') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
            <div class="row form-group">
                <div class="col-md-6">
                    <label for="job_description" class="text-uppercase"><strong>Job Description</strong> <span style="color:red;">*</span></label>
                    <textarea class="form-control col-md-12 expand {{ $errors->has('job_description') ? ' is-invalid' : '' }}" cols="40" id="job_description" name="job_description" placeholder="Add details describing the role and help potential applicants learn what makes this a great opportunity." rows="2" style="height: 82px; overflow: hidden; padding-top: 0px; padding-bottom: 0px; z-index: auto; position: relative; line-height: 20px; font-size: 14px; transition: none; background: transparent !important;" spellcheck="true">{{ old('job_description') }}</textarea>
                    <grammarly-btn>
                        <div class="_1BN1N Kzi1t MoE_1 _2DJZN" style="z-index: 2; transform: translate(354.8px, 144px);">
                            <div class="_1HjH7">
                                <div title="Protected by Grammarly" class="_3qe6h">&nbsp;</div>
                            </div>
                        </div>
                    </grammarly-btn>

                    @if ($errors->has('job_description'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('job_description') }}</strong>
                        </span>
                    @endif

                </div>

                <div class="col-md-3">
                    <label for="mandatory_skills" class="text-uppercase">Mandatory Skills<span class="tooltip-r" data-toggle="tooltip" data-placement="left" title="To help us match the best profiles, please select one or more sub-category which are important for this assignment"></span><span style="color:red;">*</span></label>
                    <select class="form-control req_field" id="mandatory_skills" name="mandatory_skills[]" multiple="multiple">
                        <option value="Activation, calendar planning">Activation, calendar planning</option>
                        <option value="Advertising/media management">Advertising/media management</option>
                        <option value="Analytics/CRM">Analytics/CRM</option>
                        <option value="Brand management">Brand management</option>
                        <option value="CSR/ Cause marketing">CSR/ Cause marketing</option>
                        <option value="Customer Loyalty &amp; Retention">Customer Loyalty &amp; Retention</option>
                        <option value="Digital marketing">Digital marketing</option>
                        <option value="External communications/public relations">External communications/public relations</option>
                        <option value="Innovation/ New product development">Innovation/ New product development</option>
                        <option value="Insights/Market research">Insights/Market research</option>
                        <option value="Marketing communications/promotions">Marketing communications/promotions</option>
                        <option value="Marketing Head/CMO functions">Marketing Head/CMO functions</option>
                        <option value="Marketing strategy and business planning">Marketing strategy and business planning</option>
                        <option value="Media Management">Media Management</option>
                        <option value="Product Management (incl product launch)">Product Management (incl product launch)</option>
                        <option value="Search Engine Optimization">Search Engine Optimization</option>
                        <option value="Social media marketing (SMM)">Social media marketing (SMM)</option>
                        <option value="Strategic pricing/ Brand P &amp; L">Strategic pricing/ Brand P &amp; L</option>
                        <option value="Trade and Shopper marketing">Trade and Shopper marketing</option>
                    </select>

                </div>
                <div class="col-md-3">
                    <label for="optional_skills" class="text-uppercase">Optional Skills<span class="tooltip-r" data-toggle="tooltip" data-placement="left" title="To help us match the best profiles, please select one or more sub-category which are important for this assignment"></span><span style="color:red;">*</span></label>
                    <select class="form-control req_field" id="optional_skills" name="optional_skills[]" multiple="multiple">
                        <option value="Activation, calendar planning">Activation, calendar planning</option>
                        <option value="Advertising/media management">Advertising/media management</option>
                        <option value="Analytics/CRM">Analytics/CRM</option>
                        <option value="Brand management">Brand management</option>
                        <option value="CSR/ Cause marketing">CSR/ Cause marketing</option>
                        <option value="Customer Loyalty &amp; Retention">Customer Loyalty &amp; Retention</option>
                        <option value="Digital marketing">Digital marketing</option>
                        <option value="External communications/public relations">External communications/public relations</option>
                        <option value="Innovation/ New product development">Innovation/ New product development</option>
                        <option value="Insights/Market research">Insights/Market research</option>
                        <option value="Marketing communications/promotions">Marketing communications/promotions</option>
                        <option value="Marketing Head/CMO functions">Marketing Head/CMO functions</option>
                        <option value="Marketing strategy and business planning">Marketing strategy and business planning</option>
                        <option value="Media Management">Media Management</option>
                        <option value="Product Management (incl product launch)">Product Management (incl product launch)</option>
                        <option value="Search Engine Optimization">Search Engine Optimization</option>
                        <option value="Social media marketing (SMM)">Social media marketing (SMM)</option>
                        <option value="Strategic pricing/ Brand P &amp; L">Strategic pricing/ Brand P &amp; L</option>
                        <option value="Trade and Shopper marketing">Trade and Shopper marketing</option>
                    </select>

                </div>

            </div>

            <div class="row form-group">
                <div class="col-md-6">
                    <label for="num_of_position" class="text-uppercase">No. of Positions <span style="color:red;">*</span></label>
                    <input class="form-control{{ $errors->has('num_of_position') ? ' is-invalid' : '' }}" id="num_of_position" min="1" name="num_of_position" type="number" value="{{ old('num_of_position') ? old('num_of_position') : 1 }}">
                    @if ($errors->has('num_of_position'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('num_of_position') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="col-md-3">
                    <div class="col-md-10 col-xs-10  noleftpadding norightpadding">
                        <label for="work_experience" class="text-uppercase ">Work Experience</label>
                    </div>

                    <div class="col-md-5 col-xs-12  noleftpadding norightpadding">
                        <input type="text" class="form-control" id="min_work_experience" name="min_work_experience" placeholder="Min" value="{{ old('min_work_experience') }}">
                    </div>
                    <div class="col-md-1 col-xs-12" style="margin:4px 4px 0px 0px; !important;"><strong>-</strong></div>
                    <div class="col-md-5 col-xs-12  noleftpadding norightpadding">
                        <input type="text" class="form-control" id="max_work_experience" name="max_work_experience" placeholder="Max" value="{{ old('max_work_experience') }}">
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="col-md-10 col-xs-10  noleftpadding norightpadding">
                        <label for="budget_amount" class="text-uppercase ">Estimated Budget</label>
                    </div>

                    <div class="col-md-5 col-xs-12  noleftpadding norightpadding">
                        <input class="form-control" id="min_budget" name="min_budget" placeholder="Min" type="text" value="{{ old('min_budget') }}">
                    </div>
                    <div class="col-md-1 col-xs-12" style="margin:4px 4px 0px 0px; !important;"><strong>-</strong></div>
                    <div class="col-md-5 col-xs-12  noleftpadding norightpadding">
                        <input class="form-control" id="max_budget" name="max_budget" placeholder="Max" type="text" value="{{ old('max_budget') }}">
                    </div>
                </div>
            </div>
            <div class="row form-group nobottommargin">
                <div class="col-md-4">
                    <label for="engagement_type" class="text-uppercase">Engagement Type<span style="color:red;">*</span></label>
                    <select class="form-control req_field" id="engagement_type" name="engagement_type">
                        <option value="">Select One</option>
                        <option value="Onsite" {{(old('engagement_type') == 'Onsite' ? 'selected':'')}}>Onsite</option>
                        <option value="Offsite" {{(old('engagement_type') == 'Offsite' ? 'selected':'')}}>Offsite</option>
                        <option value="Both (On-Site and offsite)" {{(old('engagement_type') == 'Both (On-Site and offsite)' ? 'selected':'')}}>Both (On-Site and offsite)</option>
                    </select>
                    @if ($errors->has('engagement_type'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('engagement_type') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="col-md-4">
                    <label for="location" class="text-uppercase">Location <span style="color:red;">*</span></label>
                    <select class="form-control input-md" id="location" name="location[]" multiple="multiple">
                        @foreach ($cities as $city)
                          <option value="{{ $city->id }}" {{ (collect(old('location'))->contains($city->id)) ? 'selected':'' }}>{{ $city->name}}</option>
                        @endforeach
                    </select>
                    @if ($errors->has('location'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('location') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="col-md-4">
                    <label for="start_date" class="text-uppercase">Start Date<span style="color:red;">*</span></label>
                    <input type="date" class="form-control" id="start_date" name="start_date" value="{{ old('start_date') }}">
                    @if ($errors->has('start_date'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('start_date') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="col-md-4 margintop">
                    <label for="end_date" class="text-uppercase">End Date </label>
                    <input type="date" class="form-control" id="end_date" name="end_date" value="{{ old('end_date') }}">
                    @if ($errors->has('end_date'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('end_date') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
            <div class="row form-group">
                <!-- /.row -->
                <div class="col-md-4">
                    <label for="employment_type" class="text-uppercase"><strong>Employment Type</strong><span style="color:red;">*</span> </label>
                    <select class="form-control" id="employment_type" name="employment_type">
                        <option value="">Select One</option>
                        <option value="Permament" {{(old('employment_type') == 'Permament' ? 'selected':'')}}>Permament</option>
                        <option value="Lease" {{(old('employment_type') == 'Lease' ? 'selected':'')}}>Lease</option>
                        <option value="Both (permament and Lease)" {{(old('employment_type') == 'Both (permament and Lease)' ? 'selected':'')}}>Both (permament and Lease)</option>
                    </select>
                     @if ($errors->has('employment_type'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('employment_type') }}</strong>
                        </span>
                    @endif
                </div>
               <div class="col-md-4">
                    <label for="industry" class="text-uppercase"><strong>Industry</strong> <span style="color:red;">*</span></label>

                    <select class="form-control req_field" id="industry" name="industry">
                        <option value="">Select Industry</option>
                        @foreach ($industries as $industry)
                          <option value="{{ $industry->id }}" {{(old('industry') == $industry->id?'selected':'')}}>{{ $industry->name}}</option>
                        @endforeach
                    </select>
                    @if ($errors->has('industry'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('industry') }}</strong>
                        </span>
                    @endif
                </div>

                <div class="col-md-4">
                    <label for="qualification" class="text-uppercase"><strong>Qualification</strong> <span style="color:red;">*</span></label>
                    <select class="form-control req_field" id="qualification" name="qualification">
                        <option value="" selected="selected">Select Qualification</option>
                        <option value="Graduate" {{(old('qualification') == 'Graduate' ? 'selected':'')}}>Graduate</option>
                        <option value="Post Graduate or Higher" {{(old('qualification') == 'Post Graduate or Higher' ? 'selected':'')}}>Post Graduate or Higher</option>
                    </select>
                    @if ($errors->has('qualification'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('qualification') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
            <div class="row form-group">
                <div class="col-md-6">
                    <label for="contact_person_name" class="text-uppercase">Contact Person Name<span style="color:red;">*</span></label>
                    <input type="text"class="form-control" id="contact_person_name" name="contact_person_name" value="{{ old('contact_person_name')}}">
                    @if ($errors->has('contact_person_name'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('contact_person_name') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="col-md-6">
                    <label for="contact_person_name" class="text-uppercase">Contact Person Mobile Number <span style="color:red;">*</span></label>
                    <input type="text"class="form-control" id="contact_person_phone" name="contact_person_phone" value="{{ old('contact_person_phone')}}">
                </div>
                @if ($errors->has('contact_person_phone'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('contact_person_phone') }}</strong>
                        </span>
                @endif
            </div>

            <div class=" form-group">
                <div class="col-md-12 nopadding text-center">
                    <input type="submit" class="btn novecento-normal post_btn_save" title="Submit your details" value="Post" style="color: #ffffff; background-color: #232370;">
                </div>
            </div>
        </div>
    </div>

    </div>
    </div>
    </div>
</form>

@endsection